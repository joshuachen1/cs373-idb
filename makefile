.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

all:

# auto format the code
format:
	black ./*.py

# check files, check their existence with make check
CFILES :=                                 \
    ./front-end/.gitignore                            \
    .gitlab-ci.yml                        

# check the existence of check files
check: $(CFILES)

# Front-end docker
build-front-docker:
	cd front-end; \
	docker build -t front-end:latest .

run-front-docker:
	cd front-end; \
	docker run --name front-end-docker -d -p 3000:3000 front-end:latest

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__


