# Testing structure based off of previous group: https://gitlab.com/10am-group-8/adopt-a-pet/-/blob/main/front-end/selenium/gui_test.py
import sys
import time
import pytest
import unittest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# unittest.TestLoader.sortTestMethodsUsing = None
url = "https://www.reliefmaps.me/" #set url to this when pushing to main
# url = "https://develop.d3ralqkm4r3zrd.amplifyapp.com/"
local = False  # set to False when pushing to gitlab
print("beginning setup for test_gui module")

# allow gitlab ci/cd to run selenium tests (pushing as local = True for now b/c I don't think we have this setup yet)
global driver, wait
chrome_options = Options()
# chrome_options.add_argument(
#     "--headless"
# )  # this sets whether you can see the webpage or not
# chrome_options.add_argument("--start-maximized")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--disable-dev-shm-usage")
chrome_options.add_argument("window-size=1200x600")
chrome_options.add_experimental_option(
    "excludeSwitches", ["enable-logging"]
)  # gets rid of an error
if local:  # if testing before pushing
    driver = webdriver.Chrome(
        service=Service(ChromeDriverManager().install()), options=chrome_options
    )
else:  # use docker
    driver = Remote(
        "http://selenium__standalone-chrome:4444/wd/hub",
        desired_capabilities=chrome_options.to_capabilities(),
    )
driver.get(url)
wait = WebDriverWait(driver, 10)


class TestGuiFrontend(unittest.TestCase):
    def test_a_title(self):
        print("starting test_title")
        self.assertEqual(
            driver.title, "Relief Map"
        )  # need to change title from React App to Relief Map to pass this

    """
    Navbar Tests
    """

    def test_b_navbar_home(self):
        print("starting test_navbar_home")
        home = driver.find_element(By.LINK_TEXT, "Home")
        home.click()
        print("driver", driver.current_url)
        self.assertEqual(driver.current_url, url)

    def test_c_navbar_about_us(self):
        print("starting test_navbar_about_us")
        about_us = driver.find_element(By.LINK_TEXT, "About Us")
        about_us.click()
        print("driver", driver.current_url)
        self.assertEqual(driver.current_url, url + "about")

    def test_d_navbar_countries(self):
        print("starting test_navbar_countries")
        countries = driver.find_element(By.LINK_TEXT, "Countries")
        countries.click()
        print("driver", driver.current_url)
        self.assertEqual(driver.current_url, url + "countries")

    def test_e_navbar_crises(self):
        print("starting test_navbar_crises")
        crises = driver.find_element(By.LINK_TEXT, "Crises")
        crises.click()
        print("driver", driver.current_url)
        self.assertEqual(driver.current_url, url + "crises")

    def test_f_navbar_charities(self):
        print("starting test_navbar_charities")
        about_us = driver.find_element(By.LINK_TEXT, "Charities")
        about_us.click()
        print("driver", driver.current_url)
        self.assertEqual(driver.current_url, url + "charities")

    """
    Filtering and Searching Tests
    """

    def test_g_country_filter(self):
        print("starting test_country_filter")
        countries = driver.find_element(By.LINK_TEXT, "Countries")
        countries.click()

        # self.assertEqual(driver.current_url, url + "countries")
        area_filter = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div/div[1]/div/div[1]/button"
        )
        self.assertEqual(area_filter.text, "Area")

    def test_h_country_search(self):
        print("starting test_country_search")
        countries = driver.find_element(By.LINK_TEXT, "Countries")
        countries.click()

        search_button = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div/div[1]/form/button"
        )
        self.assertEqual(search_button.text, "Search")

    def test_i_crisis_filter(self):
        print("starting test_crisis_filter")
        crises = driver.find_element(By.LINK_TEXT, "Crises")
        crises.click()

        area_filter = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div/div[1]/div/div[1]/button"
        )
        self.assertEqual(area_filter.text, "Area")

    def test_j_crisis_search(self):
        print("starting test_crisis_search")
        crises = driver.find_element(By.LINK_TEXT, "Crises")
        crises.click()

        search_button = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div/div[1]/form/button"
        )
        self.assertEqual(search_button.text, "Search")

    def test_k_charity_filter(self):
        print("starting test_charity_filter")
        charities = driver.find_element(By.LINK_TEXT, "Charities")
        charities.click()

        country_filter = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div/div[1]/div/div[1]/button"
        )
        self.assertEqual(country_filter.text, "Country")

    def test_l_charity_search(self):
        print("starting test_charity_search")
        charities = driver.find_element(By.LINK_TEXT, "Charities")
        charities.click()

        search_button = driver.find_element(
            By.XPATH, "/html/body/div[1]/div/div/div[1]/form/button"
        )
        self.assertEqual(search_button.text, "Search")
        driver.quit()  # for last test


if __name__ == "__main__":
    unittest.main()
