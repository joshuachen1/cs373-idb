//TODO: (Just an example). Add the fields we are interested in
export type Charity = {
    name: string;
    countries: string;
    need: string;
    donations: number;
    report: string;
};


export const charities: Charity[] = [
    {
        name: "AFCAids",
        countries: "Democratic Republic of the Congo, Kenya, Malawi, Uganda, Zimbabwe",
        need: "Food, Animals",
        donations: 749,
        report: "2022-06-21",
    },

    {
        name: "RI",
        countries: "Afghanistan, Bangladesh, Ghana, Iran, Iraq, Jordan, Lebanon, Myanmar, Pakistan, Somalia, Sudan, United States, Yemen, South Sudan",
        need: "Food",
        donations: 1109,
        report: "2021-02-13",
    },

    {
        name: "Kids for Kids",
        countries: "Sudan",
        need: "Food, Goats",
        donations: 1317,
        report: "2022-07-06",
    },
]