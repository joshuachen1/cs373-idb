//TODO: (Just an example). Add the fields we are interested in
export type Crises = {
    name: string;
    type: string;
    country: string;
    starting: string;
    last_update: string;
};


export const crises: Crises[] = [
    {
        name: "Zimbabwe-Epidemic",
        type: "Epidemic",
        country: "Zimbabwe",
        starting: "2022-09-15",
        last_update: "2022-09-15",
    },

    {
        name: "Yemen-Flood",
        type: "Flood",
        country: "Yemen",
        starting: "2022-07-14",
        last_update: "2022-09-15",
    },

    {
        name: "Sudan-Flood",
        type: "Flood",
        country: "Sudan",
        starting: "2022-07-14",
        last_update: "2022-09-19",
    },
]