//TODO: (Just an example). Add the fields we are interested in
export type Country = {
    name: string;
    capital: string;
    area: string;
    population: number;
    un: string;
    bordered: string;
};


export const countries: Country[] = [
    {
        name: "Republic of Zimbabwe",
        capital: "Harare",
        area: "Southern Africa",
        population: 14862927,
        un: "true",
        bordered: "BWA,MOZ,ZAF,ZMB",
    },

    {
        name: "Republic of Yemen",
        capital: "Sana'a",
        area: "Western Asia",
        population: 29825968,
        un: "true",
        bordered: "OMN,SAU",
    },

    {
        name: "Republic of South Sudan",
        capital: "Juba",
        area: "Middle Africa",
        population: 11193729,
        un: "true",
        bordered: "CAF,COD,ETH,KEN,SDN,UGA",
    },
]