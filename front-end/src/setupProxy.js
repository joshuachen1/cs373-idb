// code from https://stackoverflow.com/questions/70374005/invalid-options-object-dev-server-has-been-initialized-using-an-options-object
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'reliefmapbackend-env.eba-zahqdvhf.us-east-1.elasticbeanstalk.com/',
      changeOrigin: true,
    })
  );
};