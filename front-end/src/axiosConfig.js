//code from https://stackoverflow.com/questions/51794553/how-do-i-create-configuration-for-axios-for-default-request-headers-in-every-htt
import axios from 'axios';
const instance = axios.create({
    baseURL: 'https://rmback-env.eba-4rfwkmin.us-east-1.elasticbeanstalk.com/'
    //baseURL: 'https://reliefmapsbackend.us-east-1.elasticbeanstalk.com/'
    // baseURL: 'http://localhost:5000'
});
// // Where you would set stuff like your 'Authorization' header, etc ...
// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

export default instance;