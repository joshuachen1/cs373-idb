import React from "react";
import { BrowserRouter } from "react-router-dom";

import Navbar from "../components/navbar/navbar";
import App from "../App";
import About from "../pages/about";
import HomePage from "../pages/home"
import Countries from "../pages/countries_page/countries"
import Crises from "../pages/crises_page/crises"
import Charities from "../pages/charities_page/charities"
import CountryInstance from "../pages/countries_page/countryinstance"
import CrisisInstance from "../pages/crises_page/crisisinstance"
import CharityInstance from "../pages/charities_page/charityinstance"

// referenced https://gitlab.com/JohnPowow/animalwatch/-/blob/main/frontend/src/__tests__/JestTests.test.js

describe("Overall rendering tests", () => {

    // Test 1
    test("App renders without crashing", () => {
      <BrowserRouter>
        render(<App />);
        expect(screen.getByLabel('navbar')).toBeInTheDocument();
      </BrowserRouter>;
    });
  
    // Test 2
    test("About Page renders without crashing", () => {
      <BrowserRouter>
        render(<About />);
        expect(screen.getByText('About us')).toBeInTheDocument();
      </BrowserRouter>;
    });
  
    // Test 3
    test("Home Page renders without crashing", () => {
      <BrowserRouter>
        render(<HomePage />);
        expect(screen.getByText('ReliefMap')).toBeInTheDocument();
      </BrowserRouter>;
    });
  
    // Test 4
    test("Countries Page renders without crashing", () => {
      <BrowserRouter>
        render(<Countries />);
        expect(screen.getByText('Countries')).toBeInTheDocument();
      </BrowserRouter>;
    });

    // Test 5
    test("Crises Page renders without crashing", () => {
    <BrowserRouter>
        render(<Crises />);
        expect(screen.getByText('Crises')).toBeInTheDocument();
    </BrowserRouter>;
    });

    // Test 6
    test("Charities Page renders without crashing", () => {
    <BrowserRouter>
        render(<Charities />);
        expect(screen.getByText('Charities')).toBeInTheDocument();
    </BrowserRouter>;
    });

    // Test 7
    test("CountryInstance Page renders without crashing", () => {
    <BrowserRouter>
        render(<CountryInstance />);
        expect(screen.getByText('Population')).toBeInTheDocument();
    </BrowserRouter>;
    });

    // Test 8
    test("CharityInstance Page renders without crashing", () => {
    <BrowserRouter>
        render(<CharityInstance />);
        expect(screen.getByText('Charity')).toBeInTheDocument();
    </BrowserRouter>;
    });

    // Test 9
    test("CrisisInstance Page renders without crashing", () => {
        <BrowserRouter>
            render(<CrisisInstance />);
            expect(screen.getByText('Disaster Type')).toBeInTheDocument();
        </BrowserRouter>;
        });

    // Test 10
    test("Navbar Rrenders without crashing", () => {
    <BrowserRouter>
        render(<Navbar />);
        expect(screen.getByText('Home')).toBeInTheDocument();
    </BrowserRouter>
    });
  });
  