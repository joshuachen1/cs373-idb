import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import AboutPage from './pages/about';
import CharitiesPage from './pages/charities_page/charities';
import CharityInstancePage from './pages/charities_page/charityinstance';
import CountriesPage from './pages/countries_page/countries';
import CountryInstancePage from './pages/countries_page/countryinstance';
import CrisesPage from './pages/crises_page/crises';
import CrisisInstancePage from './pages/crises_page/crisisinstance';
import HomePage from './pages/home';
import NCAAVisualization from './pages/visualization/NCAAVisualization';
import Visualization from './pages/visualization/Visualization';
import SearchAll from './pages/searchall';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/charities" element={<CharitiesPage />} />
        <Route path="/charities/:id" element={<CharityInstancePage />} />
        <Route path="/countries" element={<CountriesPage />} />
        <Route path="/countries/:id" element={<CountryInstancePage/>} />
        <Route path="/crises" element={<CrisesPage />} />
        <Route path="/crises/:id" element={<CrisisInstancePage/>} />
        <Route path="/ncaavisualization" element={<NCAAVisualization />} />
        <Route path="/visualization" element={<Visualization />} />
        <Route path="/searchall" element={<SearchAll />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
