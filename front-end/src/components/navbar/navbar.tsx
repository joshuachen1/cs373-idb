import { Nav, Navbar } from 'react-bootstrap';
import logo from '../../media/reliefmap.png';
import 'bootstrap/dist/css/bootstrap.min.css';  

function MainNavBar() {
  return (
      <Navbar bg="primary" expand="md" className="navbar navbar-dark"> 
        <Navbar.Brand style={{paddingLeft: '8px'}}>
          <img src={logo} width="40px" height="40px" alt="Relief Map logo" />{' '}
          Relief Map
        </Navbar.Brand>

        <Navbar.Toggle className="coloring" />
        <Navbar.Collapse>
          <Nav>
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/about">About Us</Nav.Link>
            <Nav.Link href="/countries">Countries</Nav.Link>
            <Nav.Link href="/crises">Crises</Nav.Link>
            <Nav.Link href="/charities">Charities</Nav.Link>
            <Nav.Link href="/visualization">Our Visualizations</Nav.Link>
            <Nav.Link href="/ncaavisualization">NCAA Visualizations</Nav.Link>
            <Nav.Link href="/searchall">Search All</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
  );
}

export default MainNavBar;
