import { Button, TextField } from "@mui/material";
import { useState } from "react";

export const SearchBar = (props: { onSearch: (open: string) => void }) => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const handleSubmit = () => {
    props.onSearch(searchTerm);
  };
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
        }}
      >
        <TextField style={{ width: "500px"}}
          label="Search..."
          onChange={(e) => {
            setSearchTerm(e.target.value);
          }}
        />
        <Button style={{ width: "100px", height: "55px"}} variant="contained" onClick={handleSubmit}>
          search
        </Button>
      </form>
    </div>
  );
};
