import {
  Grid,
  TableCell,
  TableRow,
  TableHead,
  TableBody,
  TableContainer,
  TablePagination,
  Table,
} from "@mui/material/";
import { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import Paper from "@mui/material/Paper";
import axiosConfig from "../../axiosConfig";
import { CrisisFilter } from "./filterCrisisTable";
import { Highlight } from "react-highlight-regex";
import "../charity-table/hlt.css";

function CrisisTable() {
  //Stores the name of all crises
  const [crises, setCrises] = useState<String[]>();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState<RegExp>();
  const [count, setCount] = useState(64);

  //Stores the crisis info (e.g. name, capital, currency)

  const handlePageChange = (event: any, newPage: any) => {
    setPage(newPage);
  };

  const handleRPPChange = (event: any) => {
    setRowsPerPage(event.target.value);
  };

  // const refreshTable = async (item: any, api: any) => {
  //   var crises;
  //   console.log(item);
  //   const result = await axiosConfig("/crises/?" + api + "=" + item);
  //   crises = result.data;
  //   for (var i = 0; i < crises.length; i++) {
  //     crises[i]["startingDate"] = crises[i]["startingDate"].split("T")[0];
  //     crises[i]["dateLastUpdated"] = crises[i]["dateLastUpdated"].split("T")[0];
  //   }

  //   setCrises(crises);
  // };

  const refreshTable = useCallback(async (api: any, searchInput: any) => {
    setSearchTerm((searchInput !== "" ? new RegExp(searchInput, "i") : undefined));
    const result = await axiosConfig(api);
    var crises = result.data
    for (var i = 0; i < crises.length; i++) {
      crises[i]["startingDate"] = crises[i]["startingDate"].split("T")[0];
      crises[i]["dateLastUpdated"] = crises[i]["dateLastUpdated"].split("T")[0];
    }
    setCrises(crises);
    setCount(crises.length)
 }, []);

  useEffect(() => {
    // for reference
    var start_idx = page * rowsPerPage;
    var stop_idx = (page + 1) * rowsPerPage;

    const fetchCrisisNames = async () => {
      const result = await axiosConfig("/crises/" + start_idx + "-" + stop_idx);
      var crises = result.data;
      for (var i = 0; i < crises.length; i++) {
        crises[i]["startingDate"] = crises[i]["startingDate"].split("T")[0];
        crises[i]["dateLastUpdated"] =
          crises[i]["dateLastUpdated"].split("T")[0];
      }
      setCrises(crises);
    };
    fetchCrisisNames();
  }, [page, rowsPerPage, setCrises]);

  return (
    <div>
      {CrisisFilter(refreshTable)}
      <Paper>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Disaster</TableCell>
                  <TableCell align="right">Disaster Type</TableCell>
                  <TableCell align="right">Country</TableCell>
                  <TableCell align="right">Start Date</TableCell>
                  <TableCell align="right">Last Update</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {crises &&
                  crises.map((crisis: any) => (
                    <TableRow
                      component={Link}
                      to={`/crises/${crisis.ID}/`}
                      key={crisis.ID}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell>
                        {" "}
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={crisis.name} />
                        ) : (
                          crisis.name
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={crisis.issueType}
                          />
                        ) : (
                          crisis.issueType
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={crisis.country} />
                        ) : (
                          crisis.country
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={crisis.startingDate}
                          />
                        ) : (
                          crisis.startingDate
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={crisis.dateLastUpdated}
                          />
                        ) : (
                          crisis.dateLastUpdated
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <TablePagination
          count={count}
          onPageChange={handlePageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleRPPChange}
        />
      </Paper>
    </div>
  );
}

export default CrisisTable;
