import { Button, Form, FormControl, Dropdown} from 'react-bootstrap';
import { TextField } from "@mui/material";
import attributes from "./filter.js";
import { useState, useEffect } from "react";

export const CrisisFilter = (refreshTable: any) => {
  const [searchInput, updateSearchInput] = useState('')
  const [latInput, updateLatInput] = useState('')
  const [longInput, updateLongInput] = useState('')

  const [search, updateSearch] = useState('')
  const [area, updateArea] = useState('')
  const [country, updateCountry] = useState('')
  const [lat, updateLat] = useState('')
  const [long, updateLong] = useState('')

  useEffect(() => {
    var url = "/crises?"

    if(search !== '') {
      url += '&like=%' + search + "%"
    }
    if(area !== '') {
        url += '&issueType=' + area
    }
    if(country !== '') {
        url += '&country=' + country
    }
    if(lat !== '') {
        url += '&latitude=' + lat
    }
    if(long !== '') {
        url += '&longitude=' + long
    }
    refreshTable(url, search)
    
}, [search, area, country, lat, long, refreshTable]);

  return (
    <div>
            <form
        onSubmit={(e) => {
          e.preventDefault();
          updateSearch(searchInput);
        }}
      >
        <TextField style={{ width: "500px"}}
          label="Search..."
          onChange={(e) => {
            updateSearchInput(e.target.value);
          }}
        />
        <Button
        className="color-btn"
        onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateSearch(searchInput)}
        >
        Search
        </Button>
      </form>
      <div  className="btn-group" style={{paddingBottom: '10px'}}>
      {/* Sort filter */}
      <Dropdown style={{paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Area
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {attributes.crises_issuetype.map((item) => (
              <Dropdown.Item onClick={(e) => {updateArea(item)}}>
                {item}
              </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Country
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {attributes.crises_countries.map((item) => (
            <Dropdown.Item onClick={(e) => {updateCountry(item)}}>
            {item}
            </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle className="color-drp" id="dropdown-basic">
            Near latitude
        </Dropdown.Toggle>
        <Dropdown.Menu >
            <Form onSubmit={e => {e.preventDefault()}}>
                <FormControl
                type="search"
                placeholder={"Latitude..."}
                aria-label="Search"
                onChange={(e) => {updateLatInput(e.target.value);}}
                // onKeyPress={event => {
                //     if (event.key === "Enter") {
                //         handleZipFilter();
                //     }
                // }}
                />
                <Button
                className="color-btn"
                onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateLat(latInput)}
                >
                Filter
                </Button>
            </Form>  
        </Dropdown.Menu>
    </Dropdown>

    
    <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle className="color-drp" id="dropdown-basic">
            Near longitude
        </Dropdown.Toggle>
        <Dropdown.Menu >
            <Form onSubmit={e => {e.preventDefault()}}>
                <FormControl
                type="search"
                placeholder={"Longitude..."}
                aria-label="Search"
                onChange={(e) => {updateLongInput(e.target.value);}}
                />
                <Button
                className="color-btn"
                onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateLong(longInput)}
                >
                Filter
                </Button>
            </Form>  
        </Dropdown.Menu>
    </Dropdown>
    </div>
    </div>
  );
};
