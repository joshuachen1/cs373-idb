import gitlab from "../../media/gitlab.png"
import postman from "../../media/postman.png"
import aws from "../../media/aws.png"
import react from "../../media/react.png"

export type Tool = {
    name: string;
    description: string;
    url: string;
    img: string;
};


export const toolProfile: Tool[] = [
    {
        name: "GitLab",
        description: "Used to manage our project",
        url: "https://gitlab.com/",
        img: gitlab
    },

    {
        name: "Amazon Web Services",
        description: "Used to host our website",
        url: "https://aws.amazon.com/",
        img: aws
    },

    {
        name: "React",
        description: "Javascript library for front-end development",
        url: "https://reactjs.org/",
        img: react
    },

    {
        name: "Postman",
        description: "Platform used to design our API",
        url: "https://www.postman.com/",
        img: postman
    }
]