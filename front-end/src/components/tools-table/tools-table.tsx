import {Row, Col, Card, Container} from 'react-bootstrap'
import "./tools-table.css"
import { toolProfile } from './tools-profile';
import { Tool } from "./tools-profile"

function ToolTable() {

    return (
    <Container>
      <Row>
      {toolProfile.map((tool: Tool) => {
        return(
          <Col key={tool.name}  className="col-md-3">
            <a href={tool.url} style={{textDecoration: 'none'}}>
                <Card style={{ height: '30rem', alignItems: 'center', borderRadius:'15px'}}>
                    <Card.Img style={{width: '85%', paddingTop:'1rem'}} variant="top" src= {tool.img} />
                    <Card.Body>
                        <Card.Title>{tool.name}</Card.Title>
                        <Card.Text>
                        {tool.description}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </a>
          </Col>
        )
      })}
      </Row>
    </Container>

  );
}

export default ToolTable
