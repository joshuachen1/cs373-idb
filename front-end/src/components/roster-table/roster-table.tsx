import {Row, Col, Card, Container} from 'react-bootstrap'
import "./roster-table.css"
import { useEffect, useState } from "react";
import { teamProfile } from './team-profile';
import { Member } from "./team-profile"

function RosterTable() {
    
    const [totalCmts, setTotalCmts] = useState<number>(0);
    const [totalIssues, setTotalIssues] = useState<number>(0);
    const [totalUnitTests, setUnitTests] = useState<number>(0);

    useEffect(() => {
      const fetchData = async () => {

        //Number of commits
        let commitResponse = await fetch("https://gitlab.com/api/v4/projects/39683187/repository/commits?per_page=500");
        let commitData = await commitResponse.json();

        teamProfile.forEach((member: Member) => {
          member.commits=0;
          member.issues=0;
        })
      
        let cmtCounter=0;

        commitData.forEach((commit: any) => {
          for (var i = 0; i < teamProfile.length; i++){
            if (teamProfile[i].aliases.includes(commit.committer_name)) {
              teamProfile[i].commits+=1;
              cmtCounter+=1;
            }
          }
        })

        //Number of issues
        let issueResponse = await fetch("https://gitlab.com/api/v4/projects/39683187/issues?per_page=100");
        let issueData = await issueResponse.json();

        let issueCounter=0;
        issueData.forEach((issue: any) => {
          for (var i = 0; i < teamProfile.length; i++){
            if (issue.state === "closed"){
              for (var j = 0; j < issue.assignees.length; j++){
                if (teamProfile[i].username === issue.assignees[j].username) {
                  teamProfile[i].issues+=1;
                  issueCounter+=1;
                }
              }
            }
          }
        })

        let numtests = 0;
        for (var i = 0; i < teamProfile.length; i++){
          numtests += teamProfile[i].unit_tests;
        }
        setTotalCmts(cmtCounter)
        setTotalIssues(issueCounter)
        setUnitTests(numtests)
      }
      
      fetchData();
    });

    return (
    <Container>
      <Row>
      {teamProfile.map((member: Member) => {
        return(
          <Col key={member.name}  className="col-md-4">
            <Card style={{ height: '50rem', alignItems: 'center', borderRadius:'15px'}}>
              <Card.Img style={{width: '85%', paddingTop:'1rem'}} variant="top" src= {member.image} />
              <Card.Body>
                <Card.Title>{member.name}</Card.Title>
                <Card.Subtitle>
                  {"Role: " + member.role}
                </Card.Subtitle>
                <Card.Text>
                  {"Bio: " + member.bio}
                </Card.Text>
                <Card.Text>
                  {"Commits Done: " + member.commits}
                </Card.Text>
                <Card.Text>
                  {"Issues Closed: " + member.issues}
                </Card.Text>
                <Card.Text>
                  {"Unit Tests Written: " + member.unit_tests}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        )
      })}
      </Row>
      <h1 style={{textAlign: "center"}}>Composite Statistics</h1>
      <p><b>{totalCmts}</b> total commits on the main branch</p>
      <p><b>{totalIssues}</b> total issues closed</p>
      <p><b>{totalUnitTests}</b> total unit tests written</p>
    </Container>

  );
}

export default RosterTable

