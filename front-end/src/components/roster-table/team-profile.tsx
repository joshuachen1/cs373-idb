import aadhikari from "../../media/aadhikari.png";
import mjohnson from "../../media/mjohnson.png"
import jchen from "../../media/jchen.png"
import knguyen from "../../media/knguyen.png"

export type Member = {
    name: string;
    role: string;
    bio: string;
    username: string;
    image: string;
    commits: number;
    issues: number;
    unit_tests: number;
    //Name on gitlab commit can vary. This is just to document all possibilities
    aliases: string[];
};


export const teamProfile: Member[] = [
    {
        name: "Asim Adhikari",
		role: "Full Stack",
        bio: "Asim is a junior computer science major. Asim is interested in this project because by compiling information about humanitarian issues, we can connect those who can help with those in need. ",
        username: "asim2002",
        image: aadhikari,
        commits: 0,
        issues: 0,
        unit_tests: 0,
        aliases: ["Asim Adhikari", "Asim", "asim2002"]
    },

    {
        name: "Joshua Chen",
		role: "Full Stack",
        bio: "Joshua is a junior in computer science with a minor in business. Joshua believes staying up to date on disasters across the world should be common knowledge and our project helps with exactly that.",
        username: "joshuachen1",
        image: jchen,
        commits: 0,
        issues: 0,
        unit_tests: 18,
        aliases: ["Joshua Chen", "Josh Chen", "Joshua", "Josh"]
    },

    {
        name: "Marlowe Johnson",
		role: "Back End",
        bio: "Marlowe is a senior computer science student. Marlowe is interested in this project because he thinks that providing sources of relief for those in need is a vital neccesity.",
        username: "msj879",
        image: mjohnson,
        commits: 0,
        issues: 0,
        unit_tests: 0,
        aliases: ["Marlowe Johnson", "Marlowe", "Marlowe S Johnson", "MJ"]
    },

    {
        name: "Khoi Nguyen",
		role: "Full Stack",
        bio: "Khoi is a junior computer science major. Khoi is interested in this project because it provides people with an easy way to stay connected on the issues the world is facing and how everyone is receiving the support they need.",
        username: "khoi-t-nguyen",
        image: knguyen,
        commits: 0,
        issues: 0,
        unit_tests: 0,
        aliases: ["Khoi Nguyen", "Khoi", "khoi-t-nguyen"]
    },

]