import { Button, Form, FormControl, Dropdown} from 'react-bootstrap';
import attributes from "./filter.js";
import { useState, useEffect } from "react";
import { TextField } from "@mui/material";

export const CharityFilter = (refreshTable: any) => {
  const [searchInput, updateSearchInput] = useState('')
  const [donationsLessInput, updateDonationsLessInput] = useState('')
  const [donationsMoreInput, updateDonationsMoreInput] = useState('')

  const [search, updateSearch] = useState('')
  const [country, updateCountry] = useState('')
  const [continent, updateContinent] = useState('')
  const [theme, updateTheme] = useState('')
  const [org, updateOrg] = useState('')
  const [donationsLess, updateDonationsLess] = useState('')
  const [donationsMore, updateDonationsMore] = useState('')

  useEffect(() => {
    var url = "/charities?"

    if(search !== '') {
      url += '&like=%' + search + "%"
    }
    if(country !== '') {
        url += '&projCountry=' + country
    }
    if(continent !== '') {
        url += '&projContinent=' + continent
    }
    if(theme !== '') {
        url += '&projTheme=' + theme
    }
    if(org !== '') {
        url += '&orgName=' + org
    }
    if(donationsLess !== '') {
      url += '&numDonationsLess=' + donationsLess
    }
    if(donationsMore !== '') {
      url += '&numDonationsGreater=' + donationsMore
    }
    refreshTable(url, search)
    
}, [search, country, continent, theme, org, donationsLess, donationsMore, refreshTable]);

  return (
    <div>
                  <form
        onSubmit={(e) => {
          e.preventDefault();
          updateSearch(searchInput);
        }}
      >
        <TextField style={{ width: "500px"}}
          label="Search..."
          onChange={(e) => {
            updateSearchInput(e.target.value);
          }}
        />
        <Button
        className="color-btn"
        onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateSearch(searchInput)}
        >
        Search
        </Button>
      </form>
      <div  className="btn-group" style={{paddingBottom: '10px'}}>
      
      <Dropdown style={{paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Country
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {attributes.charities_countries.map((item) => (
              <Dropdown.Item onClick={(e) => {updateCountry(item)}}>
                {item}
              </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Continent
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {attributes.charities_continents.map((item) => (
            <Dropdown.Item onClick={(e) => {updateContinent(item)}}>
            {item}
            </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Theme
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {attributes.charities_themes.map((item) => (
            <Dropdown.Item onClick={(e) => {updateTheme(item)}}>
            {item}
            </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Organization
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {attributes.charities_orgs.map((item) => (
            <Dropdown.Item onClick={(e) => {updateOrg(item)}}>
            {item}
            </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle className="color-drp" id="dropdown-basic">
            Donations Less Than
        </Dropdown.Toggle>
        <Dropdown.Menu >
            <Form onSubmit={e => {e.preventDefault()}}>
                <FormControl
                type="search"
                placeholder={"Donations..."}
                aria-label="Search"
                onChange={(e) => {updateDonationsLessInput(e.target.value);}}
                // onKeyPress={event => {
                //     if (event.key === "Enter") {
                //         handleZipFilter();
                //     }
                // }}
                />
                <Button
                className="color-btn"
                onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateDonationsLess(donationsLessInput)}
                >
                Filter
                </Button>
            </Form>  
        </Dropdown.Menu>
    </Dropdown>

    
    <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle className="color-drp" id="dropdown-basic">
            Donations More Than
        </Dropdown.Toggle>
        <Dropdown.Menu >
            <Form onSubmit={e => {e.preventDefault()}}>
                <FormControl
                type="search"
                placeholder={"Donations..."}
                aria-label="Search"
                onChange={(e) => {updateDonationsMoreInput(e.target.value);}}
                />
                <Button
                className="color-btn"
                onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateDonationsMore(donationsMoreInput)}
                >
                Filter
                </Button>
            </Form>  
        </Dropdown.Menu>
    </Dropdown>
    </div>
    </div>
  );
};
