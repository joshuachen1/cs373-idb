import {
  Grid,
  TableCell,
  TableRow,
  TableHead,
  TableBody,
  TableContainer,
  TablePagination,
  Table,
} from "@mui/material/";
import { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import Paper from "@mui/material/Paper";
import axiosConfig from "../../axiosConfig";
import { CharityFilter } from "./filterCharityTable";
import { Highlight } from "react-highlight-regex";
import "./hlt.css";

function CharityTable() {
  //Stores the name of all charities
  const [charities, setCharities] = useState<String[]>();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState<RegExp>();
  const [count, setCount] = useState(1424);

  //Stores the charity info (e.g. name, capital, currency)

  const handlePageChange = (event: any, newPage: any) => {
    setPage(newPage);
  };

  const handleRPPChange = (event: any) => {
    setRowsPerPage(event.target.value);
  };

  // const refreshTable = async (item: any, api: any) => {
  //   var charities;
  //   console.log(item);
  //   const result = await axiosConfig("/charities/?" + api + "=" + item);
  //   charities = result.data;

  //   setCharities(charities);
  // };

  const refreshTable = useCallback(async (api: any, searchInput: any) => {
    setSearchTerm((searchInput !== "" ? new RegExp(searchInput, "i") : undefined));
    console.log(api)
    const result = await axiosConfig(api);
    setCharities(result.data);
    setCount(result.data.length);
 }, []);

  useEffect(() => {
    // for reference
    var start_idx = page * rowsPerPage;
    var stop_idx = (page + 1) * rowsPerPage;

    const fetchCharityNames = async () => {
      const result = await axiosConfig(
        "/charities/" + start_idx + "-" + stop_idx
      );

      var charities = result.data;
      setCharities(charities);
    };
    fetchCharityNames();
  }, [page, rowsPerPage, setCharities]);

  return (
    <div className="charityTable">
      {CharityFilter(refreshTable)}
      <Paper>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Country</TableCell>
                  <TableCell align="right">Number of Donations</TableCell>
                  <TableCell align="right">Project Theme</TableCell>
                  <TableCell align="right">Project Title</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {charities &&
                  charities.map((charity: any) => (
                    <TableRow
                      component={Link}
                      to={`/charities/${charity.projectID}/`}
                      key={charity.projectID}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell>
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.orgName}
                          />
                        ) : (
                          charity.orgName
                        )}
                      </TableCell>
                      <TableCell align="right">
                      {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.projCountry}
                          />
                        ) : (
                          charity.projCountry
                        )}
                      </TableCell>
                      <TableCell align="right">
                      {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.numDonations}
                          />
                        ) : (
                          charity.numDonations
                        )}
                      </TableCell>
                      <TableCell align="right"> {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.projTheme}
                          />
                        ) : (
                          charity.projTheme
                        )} </TableCell>
                      <TableCell align="right">{searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.projTitle}
                          />
                        ) : (
                          charity.projTitle
                        )}</TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <TablePagination
          count={count}
          onPageChange={handlePageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleRPPChange}
        />
      </Paper>
    </div>
  );
}

export default CharityTable;
