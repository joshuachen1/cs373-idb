import {
  Grid,
  TableCell,
  TableRow,
  TableHead,
  TableBody,
  TableContainer,
  TablePagination,
  Table,
} from "@mui/material/";
import { useEffect, useState, useCallback } from "react";
import { Link } from "react-router-dom";
import Paper from "@mui/material/Paper";
import axiosConfig from "../../axiosConfig";
import { CountryFilter } from "./filterCountryTable";
import { comma_format } from "../../utility/utility";
import { Highlight } from "react-highlight-regex";
import "../charity-table/hlt.css";

//Pagination example I looked at https://codesandbox.io/s/jolly-dubinsky-bhoi8?file=/src/App.js

function CountryTable() {
  //Stores the name of all countries
  const [countries, setCountries] = useState<String[]>();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState<RegExp>();
  const [count, setCount] = useState(250);

  //Stores the country info (e.g. name, capital, currency)

  const handlePageChange = (event: any, newPage: any) => {
    setPage(newPage);
  };

  const handleRPPChange = (event: any) => {
    setRowsPerPage(event.target.value);
  };

  const refreshTable = useCallback(async (api: any, searchInput: any) => {
    setSearchTerm((searchInput !== "" ? new RegExp(searchInput, "i") : undefined));
    const result = await axiosConfig(api);
    setCountries(result.data);
    setCount(result.data.length)
 }, []);

  useEffect(() => {
    // for reference
    var start_idx = page * rowsPerPage;
    var stop_idx = (page + 1) * rowsPerPage;

    const fetchCountryNames = async () => {
      const result = await axiosConfig(
        "/countries/" + start_idx + "-" + stop_idx
      );
      var sorted_countries = result.data; //already in order
      setCountries(sorted_countries);
    };
    fetchCountryNames();
  }, [page, rowsPerPage, setCountries]);

  return (
    <div>
      {CountryFilter(refreshTable)}
      <Paper>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Capital</TableCell>
                  <TableCell align="right">Population</TableCell>
                  <TableCell align="right">UN Member</TableCell>
                  <TableCell align="right">Area</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {countries &&
                  countries.map((country: any) => (
                    <TableRow
                      component={Link}
                      to={`/countries/${country.name}/`}
                      key={country.name}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell>
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={country.name} />
                        ) : (
                          country.name
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={country.capital.substring(
                              2,
                              country.capital.length - 2
                            )}
                          />
                        ) : (
                          country.capital.substring(
                            2,
                            country.capital.length - 2
                          )
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={comma_format(country.population)}
                          />
                        ) : (
                          comma_format(country.population)
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={country.unMember ? "Yes" : "No"}
                          />
                        ) : country.unMember ? (
                          "Yes"
                        ) : (
                          "No"
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={country.area} />
                        ) : (
                          country.area
                        )}{" "}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <TablePagination
          count={count}
          onPageChange={handlePageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleRPPChange}
        />
      </Paper>
    </div>
  );
}

export default CountryTable;
