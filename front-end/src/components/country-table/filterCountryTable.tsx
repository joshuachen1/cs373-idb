// import Dropdown from "react-bootstrap/Dropdown";
import { Button, Form, FormControl, Dropdown} from 'react-bootstrap';
import { TextField } from "@mui/material";
import attributes from "./filter.js";
import { useState, useEffect } from "react";

//Credit to https://gitlab.com/valavalkar/queue/-/blob/main/www.chorus-queue.com/src/components/locationmodel.tsx for dropdown form idea for population
//and for multiple filters code where we use states


// const [popInput, updatePopInput] = useState('')

// const handlePopFilterInput = (pop: string) => {
//   console.log(pop)
//   updatePopInput(pop)
// }

export const CountryFilter = (refreshTable: any) => {
  const [searchInput, updateSearchInput] = useState('')
  const [popLessInput, updatePopLessInput] = useState('')
  const [popMoreInput, updatePopMoreInput] = useState('')

  const [search, updateSearch] = useState('')
  const [area, updateArea] = useState('')
  const [country, updateCountry] = useState('')
  const [UN, updateUN] = useState('')
  const [borderedBy, updateBorderedBy] = useState('')
  const [popLess, updatePopLess] = useState('')
  const [popMore, updatePopMore] = useState('')

  useEffect(() => {
    var url = "/countries?"

    if(search !== '') {
      url += '&like=%' + search + "%"
    }
    if(area !== '') {
        url += '&area=' + area
    }
    if(country !== '') {
        url += '&country=' + country
    }
    if(UN !== '') {
        url += '&unMember=' + UN
    }
    if(borderedBy !== '') {
        url += '&borderedBy=' + borderedBy
    }
    if(popLess !== '') {
      url += '&populationLess=' + popLess
    }
    if(popMore !== '') {
      url += '&populationGreater=' + popMore
    }
    refreshTable(url, search)
    
}, [search, area, country, UN, borderedBy, popLess, popMore, refreshTable]);

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          updateSearch(searchInput);
        }}
      >
        <TextField style={{ width: "500px"}}
          label="Search..."
          onChange={(e) => {
            updateSearchInput(e.target.value);
          }}
        />
        <Button
        className="color-btn"
        onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updateSearch(searchInput)}
        >
        Search
        </Button>
      </form>
      <div  className="btn-group" style={{paddingBottom: '10px'}}>
      {/* Sort filter */}
      <Dropdown style={{paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Area
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {attributes.country_areas.map((item) => (
              <Dropdown.Item onClick={(e) => {updateArea(item)}}>
                {item}
              </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Bordered by
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {attributes.country_borders.map((item) => (
              <Dropdown.Item onClick={(e) => {updateBorderedBy(item)}}>
                {item}
              </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          Country
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {attributes.country_names.map((item) => (
            <Dropdown.Item onClick={(e) => {updateCountry(item)}}>
            {item}
            </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
          UN
        </Dropdown.Toggle>
        <Dropdown.Menu>
          {attributes.country_un_options.map((item) => (
            <Dropdown.Item onClick={(e) => {updateUN(item)}}>
            {item}
            </Dropdown.Item>
          ))}
          
        </Dropdown.Menu>
      </Dropdown>

      <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle className="color-drp" id="dropdown-basic">
            Population Less Than
        </Dropdown.Toggle>
        <Dropdown.Menu >
            <Form onSubmit={e => {e.preventDefault()}}>
                <FormControl
                type="search"
                placeholder={"Less Than..."}
                aria-label="Search"
                onChange={(e) => {updatePopLessInput(e.target.value);}}
                // onKeyPress={event => {
                //     if (event.key === "Enter") {
                //         handleZipFilter();
                //     }
                // }}
                />
                <Button
                className="color-btn"
                onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updatePopLess(popLessInput)}
                >
                Filter
                </Button>
            </Form>  
        </Dropdown.Menu>
    </Dropdown>

    
    <Dropdown style={{paddingLeft: '2px', paddingRight: '2px'}}>
        <Dropdown.Toggle className="color-drp" id="dropdown-basic">
            Population More Than
        </Dropdown.Toggle>
        <Dropdown.Menu >
            <Form onSubmit={e => {e.preventDefault()}}>
                <FormControl
                type="search"
                placeholder={"Greater than..."}
                aria-label="Search"
                onChange={(e) => {updatePopMoreInput(e.target.value);}}
                />
                <Button
                className="color-btn"
                onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => updatePopMore(popMoreInput)}
                >
                Filter
                </Button>
            </Form>  
        </Dropdown.Menu>
    </Dropdown>
    </div>
    </div>
  );
};
