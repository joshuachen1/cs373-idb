import reliefweb from "../../media/reliefweb.png"
import restcountries from "../../media/restcountries.png"
import globalgiving from "../../media/globalgiving.png"
import reliefmap from "../../media/reliefmap.png"

export type API = {
    name: string;
    description: string;
    url: string;
    img: string;
};


export const apiProfile: API[] = [
    {
        name: "ReliefMap",
        description: "Source for data about countries, crises in them, and ways to donate.",
        url: "https://documenter.getpostman.com/view/23609847/2s83tJGqYj",
        img: reliefmap
    },

    {
        name: "ReliefWeb",
        description: "Source for data related to ongoing disasters. Currently, we statically scraped/recorded data from reliefweb to use for our website.",
        url: "https://reliefweb.int/",
        img: reliefweb
    },

    {
        name: "REST Countries",
        description: "Source for data related to world countries. Currently, we statically scraped/recorded data from REST Countries to use for our website.",
        url: "https://restcountries.com/",
        img: restcountries
    },

    {
        name: "Global Giving",
        description: "Source for data related to charities/relief organizations. Currently, we statically scraped/recorded data from Global Giving to use for our website.",
        url: "https://www.globalgiving.org/api/methods/",
        img: globalgiving
    }
]