import {Row, Col, Card, Container} from 'react-bootstrap'
import "./api-table.css"
import { apiProfile } from './api-profile';
import { API } from "./api-profile"

function APITable() {

    return (
    <Container>
      <Row>
      {apiProfile.map((api: API) => {
        return(
          <Col key={api.name}  className="col-md-3">
            <a href={api.url} style={{textDecoration: 'none'}}>
                <Card style={{ height: '30rem', alignItems: 'center', borderRadius:'15px'}}>
                    <Card.Img style={{width: '85%', paddingTop:'1rem'}} variant="top" src= {api.img} />
                    <Card.Body>
                        <Card.Title>{api.name}</Card.Title>
                        <Card.Text>
                        {api.description}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </a>
          </Col>
        )
      })}
      </Row>
    </Container>

  );
}

export default APITable
