import MainNavBar from '../components/navbar/navbar';

function APIPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>API documentation</h1>
    </div>
  );
}

export default APIPage;
