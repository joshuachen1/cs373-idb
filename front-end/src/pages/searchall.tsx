import {
    Grid,
    TableCell,
    TableRow,
    TableHead,
    TableBody,
    TableContainer,
    TablePagination,
    Table,
  } from "@mui/material/";
  import MainNavBar from '../components/navbar/navbar';
  import { useState } from "react";
  import { Link } from "react-router-dom";
  import Paper from "@mui/material/Paper";
  import axiosConfig from "../axiosConfig";
  import { SearchBar } from "../components/navbar/searchBar";
  import { comma_format } from "../utility/utility";
  import { Highlight } from "react-highlight-regex";

function SearchAllPage() {
  const [countries, setCountries] = useState<String[]>();
  const [crises, setCrises] = useState<String[]>();
  const [charities, setCharities] = useState<String[]>();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState<RegExp>();

  const handlePageChange = (event: any, newPage: any) => {
    setPage(newPage);
  };

  const handleRPPChange = (event: any) => {
    setRowsPerPage(event.target.value);
  };

  // searching
  const handleSubmit = async (item: any) => {
    const country_result = await axiosConfig("/countries/?like=%" + item + "%");
    const crisis_result = await axiosConfig("/crises/?like=%" + item + "%");
    const charity_result = await axiosConfig("/charities/?like=%" + item + "%");
    setSearchTerm(item !== "" ? new RegExp(item, "i") : undefined);

    setCountries(country_result.data)
    setCrises(crisis_result.data)
    setCharities(charity_result.data)
  };

  return (
    <div>
      <MainNavBar />
      <SearchBar onSearch={handleSubmit} />
      <Paper>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Capital</TableCell>
                  <TableCell align="right">Population</TableCell>
                  <TableCell align="right">UN Member</TableCell>
                  <TableCell align="right">Area</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {countries &&
                  countries.map((country: any) => (
                    <TableRow
                      component={Link}
                      to={`/countries/${country.name}/`}
                      key={country.name}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell>
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={country.name} />
                        ) : (
                          country.name
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={country.capital.substring(
                              2,
                              country.capital.length - 2
                            )}
                          />
                        ) : (
                          country.capital.substring(
                            2,
                            country.capital.length - 2
                          )
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={comma_format(country.population)}
                          />
                        ) : (
                          comma_format(country.population)
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={country.unMember ? "Yes" : "No"}
                          />
                        ) : country.unMember ? (
                          "Yes"
                        ) : (
                          "No"
                        )}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={country.area} />
                        ) : (
                          country.area
                        )}{" "}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <TablePagination
          count={100}
          onPageChange={handlePageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleRPPChange}
        />
      </Paper>


      <Paper>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Disaster</TableCell>
                  <TableCell align="right">Disaster Type</TableCell>
                  <TableCell align="right">Country</TableCell>
                  <TableCell align="right">Start Date</TableCell>
                  <TableCell align="right">Last Update</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {crises &&
                  crises.map((crisis: any) => (
                    <TableRow
                      component={Link}
                      to={`/crises/${crisis.ID}/`}
                      key={crisis.ID}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell>
                        {" "}
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={crisis.name} />
                        ) : (
                          crisis.name
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={crisis.issueType}
                          />
                        ) : (
                          crisis.issueType
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight match={searchTerm} text={crisis.country} />
                        ) : (
                          crisis.country
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {" "}
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={crisis.startingDate}
                          />
                        ) : (
                          crisis.startingDate
                        )}{" "}
                      </TableCell>
                      <TableCell align="right">
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={crisis.dateLastUpdated}
                          />
                        ) : (
                          crisis.dateLastUpdated
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <TablePagination
          count={100}
          onPageChange={handlePageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleRPPChange}
        />
      </Paper>

      <Paper>
        <Grid container justifyContent="center">
          <TableContainer component={Paper} style={{ width: "80%" }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Country</TableCell>
                  <TableCell align="right">Number of Donations</TableCell>
                  <TableCell align="right">Project Theme</TableCell>
                  <TableCell align="right">Project Title</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {charities &&
                  charities.map((charity: any) => (
                    <TableRow
                      component={Link}
                      to={`/charities/${charity.projectID}/`}
                      key={charity.projectID}
                      style={{ textDecoration: "none" }}
                    >
                      <TableCell>
                        {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.orgName}
                          />
                        ) : (
                          charity.orgName
                        )}
                      </TableCell>
                      <TableCell align="right">
                      {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.projCountry}
                          />
                        ) : (
                          charity.projCountry
                        )}
                      </TableCell>
                      <TableCell align="right">
                      {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.numDonations}
                          />
                        ) : (
                          charity.numDonations
                        )}
                      </TableCell>
                      <TableCell align="right"> {searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.projTheme}
                          />
                        ) : (
                          charity.projTheme
                        )} </TableCell>
                      <TableCell align="right">{searchTerm ? (
                          <Highlight
                            match={searchTerm}
                            text={charity.projTitle}
                          />
                        ) : (
                          charity.projTitle
                        )}</TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>

        <TablePagination
          count={100}
          onPageChange={handlePageChange}
          page={page}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleRPPChange}
        />
      </Paper>
    </div>
  );
}

export default SearchAllPage;
