import { useEffect, useState } from 'react';
import { Box, Typography, Grid } from '@mui/material';
import { PieChart, Pie, Tooltip} from 'recharts';

//referenced https://gitlab.com/mrscottwlai/cs373-idb/-/blob/dev/front-end/src/visualizations/PlantEconomicUsesVisualization.tsx

const cached_data = [
    { name: 'Epidemic' , value: 17 },
	{ name: 'Drought' , value: 12 },
	{ name: 'Insect Infestation' , value: 1 },
	{ name: 'Flood' , value: 23 },
	{ name: 'Earthquake' , value: 4 },
	{ name: 'Land Slide' , value: 1 },
	{ name: 'Other' , value: 1 },
	{ name: 'Tropical Cyclone' , value: 3 }
]


const CrisesTypeVisualization = () => {
    const [data, setData] = useState<any[]>([]);

    let renderLabel = function(entry:any) {
        return entry.name;
    }

    useEffect(() => {
        setData(cached_data)
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Crises Issue Type Makeup
            </Typography>
            <Grid container justifyContent="center">
                {(
                    <PieChart width={1000} height={500}>
                        <Pie
                            data={data}
                            dataKey="value"
                            nameKey="name"
                            fill="#8B4000"
                            legendType = "line"
                            label = {renderLabel}
                        />
                        <Tooltip />
                  </PieChart>
        
                )}
            </Grid>
        </Box>

    );
};

export default CrisesTypeVisualization;
