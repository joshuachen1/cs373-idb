import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { Card } from 'react-bootstrap';
import { TextField, MenuItem, Box, Typography, Grid } from "@mui/material";
import React from 'react';
import {
  XAxis,
  YAxis,
  ScatterChart,
  Scatter,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

import Spinner from 'react-bootstrap/Spinner';

//referenced https://gitlab.com/huanyu_bing/cs373-idb/-/blob/development/frontend/src/pages/visulization/CountryVisualization.tsx
// and //referenced https://gitlab.com/mrscottwlai/cs373-idb/-/blob/dev/front-end/src/visualizations/PlantEconomicUsesVisualization.tsx

interface CountryProps{
    area: string;
    borderedBy: string;
    capital: string;
    capitalLatLong: string;
    flag: string;
    interactiveMap: string;
    name: string;
    population: number;
    unMember: string;
}

function CountryPopulationVisualization(props: any){
    let [currentState, setCurrentState] = React.useState<string>("Western Europe");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    const country_areas = ['Western Europe', 'Western Asia', 'Western Africa', 'Southern Europe', 'Southern Asia', 'Southern Africa', 'Southeast Europe', 'South-Eastern Asia', 'South America', 'Polynesia', 'not part of a subregion', 'Northern Europe', 'Northern Africa', 'North America', 'Middle Africa', 'Micronesia', 'Melanesia', 'Eastern Europe', 'Eastern Asia', 'Eastern Africa', 'Central Europe', 'Central Asia', 'Central America', 'Caribbean', 'Australia and New Zealand']
    useEffect(() => {
      const getData = async () => {
        let countryResponse: AxiosResponse<any, any> = await axios.get(
          'https://rmback-env.eba-4rfwkmin.us-east-1.elasticbeanstalk.com/countries/?area=' + currentState
        );
        let countries = countryResponse.data;
        let data = process_data(countries);
        // console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
        }, [currentState]);
        if (data.length > 0){
        visualization = scatter_plot(data);
        }
      
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        Population of countries in specific regions
        </Card.Body>
        <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Region"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "16px",
            backgroundColor: "white",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {country_areas.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (countries: CountryProps[]) =>{
    let result :any[] = [];
    for (let country of countries) {
        //parsing int b/c numDonations is stored as a string
        // console.log(charity.numDonations)
        // console.log(parseInt(charity.numDonations))
        result.push({name: country.name, population: country.population / 1000});
    }
    return result;
}

const scatter_plot = (data: any[]) =>{
    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Countries by population (in the thousands) per region
            </Typography>
            <Grid container justifyContent="center">
                {(
                    <ScatterChart width={1000} height={400}>
                        <CartesianGrid />
                        <XAxis type="category" dataKey="name" name="Country" />
                        <YAxis type="number" dataKey="population" name="Population" />
                        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                        <Legend />
                        <Scatter name="Country" data={data} fill="#198754" />
                    </ScatterChart>
                )}
            </Grid>
        </Box>
    );
}

export default CountryPopulationVisualization