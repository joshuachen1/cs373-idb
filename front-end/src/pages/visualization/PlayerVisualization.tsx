import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { Card } from 'react-bootstrap';
import { TextField, MenuItem } from "@mui/material";
import React from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import Spinner from 'react-bootstrap/Spinner';

//referenced https://gitlab.com/huanyu_bing/cs373-idb/-/blob/development/frontend/src/pages/visulization/CountryVisualization.tsx

interface PlayerProps{
    conf: string;
    first_down: number;
    first_name: string;
    height: number;
    home_city: string;
    home_country: string;
    home_county_fips: string;
    home_latitude: string;
    home_longitude: string;
    home_state: string;
    jersey: number;
    last_name: string;
    name: string;
    overall: number;
    passing: number;
    passing_downs: number;
    photo1: string;
    photo2: string;
    player_id: number;
    position: string;
    rush: number;
    season: number;
    second_down: number;
    standard_downs: number;
    team: string;
    third_down: number;
    weight: number;
    year: number;
}

function PlayerVisualization(props: any){
    const team_names = ['Air Force', 'Akron', 'Alabama', 'Appalachian State', 'Arizona', 'Arizona State', 'Arkansas', 'Arkansas State', 'Army', 'Auburn', 'BYU', 'Ball State', 'Baylor', 'Boise State', 'Boston College', 'Bowling Green', 'Buffalo', 'California', 'Central Michigan', 'Charlotte', 'Cincinnati', 'Clemson', 'Coastal Carolina', 'Colorado', 'Colorado State', 'Duke', 'East Carolina', 'Eastern Michigan', 'Florida', 'Florida Atlantic', 'Florida State', 'Fresno State', 'Georgia', 'Georgia Southern', 'Georgia State', 'Georgia Tech', 'Houston', 'Illinois', 'Indiana', 'Iowa', 'Iowa State', 'James Madison', 'Kansas', 'Kansas State', 'Kent State', 'Kentucky', 'LSU', 'Liberty', 'Louisiana', 'Louisiana Tech', 'Louisville', 'Marshall', 'Maryland', 'Memphis', 'Miami', 'Miami (OH)', 'Michigan', 'Michigan State', 'Middle Tennessee', 'Minnesota', 'Mississippi State', 'Missouri', 'NC State', 'Navy', 'Nebraska', 'Nevada', 'New Mexico', 'New Mexico State', 'North Carolina', 'North Texas', 'Northern Illinois', 'Northwestern', 'Notre Dame', 'Ohio', 'Ohio State', 'Oklahoma', 'Oklahoma State', 'Old Dominion', 'Ole Miss', 'Oregon', 'Oregon State', 'Penn State', 'Pittsburgh', 'Purdue', 'Rice', 'Rutgers', 'SMU', 'San Diego State', 'South Alabama', 'South Carolina', 'South Florida', 'Stanford', 'Syracuse', 'TCU', 'Temple', 'Tennessee', 'Texas', 'Texas A&M', 'Texas State', 'Texas Tech', 'Toledo', 'Troy', 'Tulane', 'Tulsa', 'UAB', 'UCF', 'UCLA', 'UMass', 'UNLV', 'USC', 'UTEP', 'Utah', 'Utah State', 'Vanderbilt', 'Virginia', 'Virginia Tech', 'Wake Forest', 'Washington', 'Washington State', 'West Virginia', 'Western Kentucky', 'Western Michigan', 'Wisconsin', 'Wyoming']
    let [currentState, setCurrentState] = React.useState<string>("Mississippi State");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    useEffect(() => {
      const getData = async () => {
        let teamResponse: AxiosResponse<any, any> = await axios.get(
          'https://api.ncaaccess.me/api/players?team=' + currentState
        );
        // console.log('https://api.ncaaccess.me/api/players?team=' + currentState)
        let teams = teamResponse.data;
        let data = process_data(teams);
        // console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
        }, [currentState]);
        if (data.length > 0){
        visualization = bar_chart(data);
        }
      
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        Overall stats for each player
        </Card.Body>
        <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Team"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "16px",
            backgroundColor: "white",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {team_names.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (players: PlayerProps[]) =>{
    let result :any[] = [];
    for (let player of players) {
        result.push({name: player.last_name, overall: player.overall});
    }
    return result;
}

const bar_chart = (data: any[]) =>{
    return(
        <ResponsiveContainer width="100%" height={500}>
        <BarChart
          data={data}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        //   onClick={(e) => {
        //     let index = e["activeTooltipIndex"]!;
        //     props.navigate(`/companies?industriesFilter=${industries[index]}`);
        //   }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tick={false}>
            <Label
              value="Player"
              position="insideBottom"
              style={{ textAnchor: "middle" }}
            />
          </XAxis>
          <YAxis>
            <Label
              angle={-90}
              value="Overall"
              position="insideLeft"
              style={{ textAnchor: "middle" }}
            />
          </YAxis>
          {/* <Tooltip payload={data} /> */}
          <Bar dataKey="overall" fill="#8B4000" />
        </BarChart>
        </ResponsiveContainer>
    );
}

export default PlayerVisualization