import { useEffect, useState } from 'react';
import { Box, Typography, Grid } from '@mui/material';
import { PieChart, Pie, Tooltip} from 'recharts';

//referenced https://gitlab.com/mrscottwlai/cs373-idb/-/blob/dev/front-end/src/visualizations/PlantEconomicUsesVisualization.tsx

const cached_data = [
    { name: 'Big 12' , value: 10 },
	{ name: 'Mid-American' , value: 12 },
	{ name: 'Mountain West' , value: 10 },
	{ name: 'Pac-12' , value: 12 },
	{ name: 'American Athletic' , value: 11 },
	{ name: 'Sun Belt' , value: 12 },
	{ name: 'Big Ten' , value: 14 },
	{ name: 'FBS Independents' , value: 6 },
    { name: 'Conference USA' , value: 9 },
	{ name: 'SEC' , value: 14 },
	{ name: 'ACC' , value: 14 }
]


const TeamVisualization = () => {
    const [data, setData] = useState<any[]>([]);

    let renderLabel = function(entry:any) {
        return entry.name;
    }

    useEffect(() => {
        setData(cached_data)
    }, []);

    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Crises Issue Type Makeup
            </Typography>
            <Grid container justifyContent="center">
                {(
                    <PieChart width={1000} height={500}>
                        <Pie
                            data={data}
                            dataKey="value"
                            nameKey="name"
                            fill="#8B4000"
                            legendType = "line"
                            label = {renderLabel}
                        />
                        <Tooltip />
                  </PieChart>
        
                )}
            </Grid>
        </Box>

    );
};

export default TeamVisualization;
