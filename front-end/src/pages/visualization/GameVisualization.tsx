import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { Card } from 'react-bootstrap';
import { TextField, MenuItem, Box, Typography, Grid } from "@mui/material";
import React from 'react';
import {
  XAxis,
  YAxis,
  ScatterChart,
  Scatter,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

import Spinner from 'react-bootstrap/Spinner';

//referenced https://gitlab.com/huanyu_bing/cs373-idb/-/blob/development/frontend/src/pages/visulization/CountryVisualization.tsx
// and referenced https://gitlab.com/mrscottwlai/cs373-idb/-/blob/dev/front-end/src/visualizations/PlantEconomicUsesVisualization.tsx

interface GameProps{
    attendance: number;
    away_conf: string;
    away_div: string;
    away_id: number;
    away_points: number;
    away_post_elo: number;
    away_pre_elo: number;
    away_team: string;
    away_win_prob: number;
    conf_game: boolean;
    excitement_index: number;
    game_id: number;
    home_conf: string;
    home_div: string;
    home_id: number;
    home_points: number;
    home_post_elo: number;
    home_pre_elo: number;
    home_team: string;
    home_win_prob: number;
    neutral_site: boolean;
    season: number;
    season_type: string;
    start_date: string;
    start_time_tbd: boolean;
    venue: string;
    venue_id: number;
    video1: string;
    video2: string;
    week: number;
}

function GameVisualization(props: any){
    let [currentState, setCurrentState] = React.useState<string>("Miami");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    const team_names = ['Air Force', 'Akron', 'Alabama', 'Appalachian State', 'Arizona', 'Arizona State', 'Arkansas', 'Arkansas State', 'Army', 'Auburn', 'BYU', 'Ball State', 'Baylor', 'Boise State', 'Boston College', 'Bowling Green', 'Buffalo', 'California', 'Central Michigan', 'Charlotte', 'Cincinnati', 'Clemson', 'Coastal Carolina', 'Colorado', 'Colorado State', 'Duke', 'East Carolina', 'Eastern Michigan', 'Florida', 'Florida Atlantic', 'Florida State', 'Fresno State', 'Georgia', 'Georgia Southern', 'Georgia State', 'Georgia Tech', 'Houston', 'Illinois', 'Indiana', 'Iowa', 'Iowa State', 'James Madison', 'Kansas', 'Kansas State', 'Kent State', 'Kentucky', 'LSU', 'Liberty', 'Louisiana', 'Louisiana Tech', 'Louisville', 'Marshall', 'Maryland', 'Memphis', 'Miami', 'Miami (OH)', 'Michigan', 'Michigan State', 'Middle Tennessee', 'Minnesota', 'Mississippi State', 'Missouri', 'NC State', 'Navy', 'Nebraska', 'Nevada', 'New Mexico', 'New Mexico State', 'North Carolina', 'North Texas', 'Northern Illinois', 'Northwestern', 'Notre Dame', 'Ohio', 'Ohio State', 'Oklahoma', 'Oklahoma State', 'Old Dominion', 'Ole Miss', 'Oregon', 'Oregon State', 'Penn State', 'Pittsburgh', 'Purdue', 'Rice', 'Rutgers', 'SMU', 'San Diego State', 'South Alabama', 'South Carolina', 'South Florida', 'Stanford', 'Syracuse', 'TCU', 'Temple', 'Tennessee', 'Texas', 'Texas A&M', 'Texas State', 'Texas Tech', 'Toledo', 'Troy', 'Tulane', 'Tulsa', 'UAB', 'UCF', 'UCLA', 'UMass', 'UNLV', 'USC', 'UTEP', 'Utah', 'Utah State', 'Vanderbilt', 'Virginia', 'Virginia Tech', 'Wake Forest', 'Washington', 'Washington State', 'West Virginia', 'Western Kentucky', 'Western Michigan', 'Wisconsin', 'Wyoming']
    useEffect(() => {
      const getData = async () => {
        let teamResponse: AxiosResponse<any, any> = await axios.get(
          'https://api.ncaaccess.me/api/games?hteam=' + currentState
        );
        let games = teamResponse.data;
        let data = process_data(games);
        // console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
        }, [currentState]);
        if (data.length > 0){
        visualization = scatter_plot(data);
        }
      
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        Predicted win probability against other teams at home
        </Card.Body>
        <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Home Team"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "16px",
            backgroundColor: "white",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {team_names.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (games: GameProps[]) =>{
    let result :any[] = [];
    for (let game of games) {
        //parsing int b/c numDonations is stored as a string
        // console.log(charity.numDonations)
        // console.log(parseInt(charity.numDonations))
        result.push({away_team: game.away_team, win_prob: game.home_win_prob});
    }
    return result;
}

const scatter_plot = (data: any[]) =>{
    return (
        <Box>
            <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
            Predicted win rate against away teams
            </Typography>
            <Grid container justifyContent="center">
                {(
                    <ScatterChart width={1000} height={400}>
                        <CartesianGrid />
                        <XAxis type="category" dataKey="away_team" name="Away Team" />
                        <YAxis type="number" dataKey="win_prob" name="Winning probability" />
                        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                        <Legend />
                        <Scatter data={data} fill="#198754" />
                    </ScatterChart>
                )}
            </Grid>
        </Box>
    );
}

export default GameVisualization