import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { Card } from 'react-bootstrap';
import { TextField, MenuItem } from "@mui/material";
import React from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import Spinner from 'react-bootstrap/Spinner';

//referenced https://gitlab.com/huanyu_bing/cs373-idb/-/blob/development/frontend/src/pages/visulization/CountryVisualization.tsx

interface CharityProps{
    projectID: string;
    orgName: string;
    orgURL: string;
    orgMission: string;
    projTitle: string;
    projSummary: string;
    projTheme: string;
    donationURL: string;
    numDonations: string;
    need: string;
    projCountry: string;
    projImage: string;
    latitude: string;
    longitude: string;
}

function CharityVisualization(props: any){
    const charity_countries = ['Madagascar', 'Nepal', 'South Sudan', 'Haiti', 'Hungary', 'Morocco', 'Sudan', 'Armenia', 'Belize', 'Mali', 'Nicaragua', 'Niger', 'Paraguay', 'Senegal', 'Virgin Islands', 'Lithuania', 'United States', 'Namibia', 'Panama', 'Pakistan', 'Uganda', 'Benin', 'Bosnia and Herzegovina', 'Kenya', 'Malaysia', 'Rwanda', 'South Africa', 'Taiwan', 'Tajikistan', 'Turkey', 'Dominican Republic', 'Syrian Arab Republic', 'Cameroon', 'Chile', 'Malawi', 'Nigeria', 'Georgia', 'France', 'Ireland', 'Canada', 'The Bahamas', 'Eswatini', 'Saint Vincent and the Grenadines', 'Cambodia', 'East Timor', 'Zambia', 'Palau', 'Burundi', 'Jordan', 'El Salvador', 'Israel', 'Jamaica', 'Lesotho', 'Serbia', 'South Korea', 'Liberia', 'Chad', 'Mexico', 'Cuba', 'Netherlands', 'Denmark', 'Iraq', 'Fiji', 'Bhutan', 'Democratic Republic of the Congo', 'Romania', 'Argentina', 'Germany', 'Kosovo', 'The Gambia', 'Venezuela', 'Ethiopia', 'Finland', 'India', 'Italy', 'Republic of Moldova', 'Sierra Leone', 'Switzerland', 'Hong Kong SAR', 'Egypt', 'Uruguay', 'Suriname', 'Spain', 'Greece', 'United Republic of Tanzania', 'Ecuador', 'Indonesia', 'Afghanistan', 'Ghana', 'Peru', 'United Kingdom', 'Colombia', 'Iceland', 'Mongolia', 'Sweden', 'Bangladesh', 'Botswana', 'Kazakhstan', 'Palestine', 'Poland', 'Saint Lucia', 'Togo', 'Albania', 'Vietnam', 'Thailand', 'Antarctica', 'Guatemala', 'Maldives', 'Puerto Rico', 'Bulgaria', 'Eritrea', 'Mozambique', "CÃ´te d'Ivoire", 'Guinea-Bissau', 'Belgium', 'Portugal', 'Brazil', 'Zimbabwe', 'Costa Rica', 'Guinea', 'Philippines', 'Ukraine', 'Myanmar', 'Bolivia', 'Lebanon', 'The Federated States of Micronesia', 'Somalia', 'Japan', 'Angola', 'Croatia', 'Sint Maarten', 'Mauritania', 'Sri Lanka', 'Yemen', 'Cyprus', 'Russia', 'Australia', 'Austria', 'North Macedonia', 'New Zealand', 'Burkina Faso', 'Seychelles', 'Honduras']
    let [currentState, setCurrentState] = React.useState<string>("Haiti");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    useEffect(() => {
      const getData = async () => {
        let charityResponse: AxiosResponse<any, any> = await axios.get(
          'https://rmback-env.eba-4rfwkmin.us-east-1.elasticbeanstalk.com/charities/?projCountry=' + currentState
        );
        let charities = charityResponse.data;
        let data = process_data(charities);
        // console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
        }, [currentState]);
        if (data.length > 0){
        visualization = bar_chart(data);
        }
      
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        Number of donations per charity project
        </Card.Body>
        <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Charity Country"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "16px",
            backgroundColor: "white",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {charity_countries.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (charities: CharityProps[]) =>{
    let result :any[] = [];
    for (let charity of charities) {
        //parsing int b/c numDonations is stored as a string
        // console.log(charity.numDonations)
        // console.log(parseInt(charity.numDonations))
        result.push({name: charity.orgName, donations: parseInt(charity.numDonations)});
    }
    return result;
}

const bar_chart = (data: any[]) =>{
    return(
        <ResponsiveContainer width="100%" height={500}>
        <BarChart
          data={data}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        //   onClick={(e) => {
        //     let index = e["activeTooltipIndex"]!;
        //     props.navigate(`/companies?industriesFilter=${industries[index]}`);
        //   }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tick={false}>
            <Label
              value="Charity"
              position="insideBottom"
              style={{ textAnchor: "middle" }}
            />
          </XAxis>
          <YAxis>
            <Label
              angle={-90}
              value="Number of donations"
              position="insideLeft"
              style={{ textAnchor: "middle" }}
            />
          </YAxis>
          {/* <Tooltip payload={data} /> */}
          <Bar dataKey="donations" fill="#8B4000" />
        </BarChart>
        </ResponsiveContainer>
    );
}

export default CharityVisualization