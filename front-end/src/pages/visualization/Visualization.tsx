import CharityVisualization from "./CharityVisualization"
import MainNavBar from '../../components/navbar/navbar';
import CrisesTypeVisualization from "./CrisesTypeVisualization";
import CountryPopulationVisualization from "./CountryPopulationVisualization";
export default function Visualization(props: any) {
    return (
        <div className="App">
          <MainNavBar />
          <h1>Charity Project Donations</h1>
          <CharityVisualization />
          <h1>Crises Issue Type Breakdown</h1>
          <CrisesTypeVisualization />
          <h1>Countries by population</h1>
          <CountryPopulationVisualization />
        </div>
      );
}