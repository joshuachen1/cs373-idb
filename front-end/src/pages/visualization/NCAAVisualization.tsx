import PlayerVisualization from "./PlayerVisualization"
import MainNavBar from '../../components/navbar/navbar';
import TeamVisualization from "./TeamVisualization";
import GameVisualization from "./GameVisualization";
export default function NCAAVisualization(props: any) {
    return (
        <div className="App">
          <MainNavBar />
          <h1>Player Stats Visualization</h1>
          <PlayerVisualization />
          <h1>Team Conference Breakdown Visualization</h1>
          <TeamVisualization />
          <h1>Game Win Chance Visualization</h1>
          <GameVisualization />
        </div>
      );
}
