import MainNavBar from '../components/navbar/navbar';
import map from "../media/worldmap.png"
import "./home.css"
import { Link } from 'react-router-dom';
import {Row, Col, Card, Container} from 'react-bootstrap'

//Images
import flags from "../media/worldflags.png"
import crises from "../media/crises.png"
import relief from "../media/relief.png"

function HomePage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1 style={{paddingTop: '10px', fontSize: '60px'}}>Relief Map</h1>
      {/* <h4><br></br></h4> */}
      <img src={map}style={{ width: "50%", height: "50%" }} alt="Map of the world with the continents as dots"></img>
      <h2>Explore and help mitigate ongoing crises in the world</h2>
      <Container>
      <Row>
          <Col className="col-md-4">
            <Link style={{textDecoration: 'none'}} to="countries">
              <Card style={{objectFit:'cover', objectPosition:'center', overflow:'hidden', height:'23rem', borderRadius:'15px'}}>
                <Card.Body>
                  <Card.Title><b>{"Countries"}</b></Card.Title>
                  <Card.Subtitle>{"Examine countries around the world"}</Card.Subtitle>
                  <Card.Img variant="bottom" src= {flags} style={{height: '16rem', width: '16rem'}} />
                </Card.Body>
              </Card>
            </Link>
          </Col>

          <Col className="col-md-4">
            <Link style={{textDecoration: 'none'}} to="crises">
              <Card style={{objectFit:'cover', objectPosition:'center', overflow:'hidden', height:'23rem', borderRadius:'15px'}}>
                <Card.Body>
                  <Card.Title><b>{"Crises"}</b></Card.Title>
                  <Card.Subtitle>{"Explore ongoing crises and disasters"}</Card.Subtitle>
                  <Card.Img variant="bottom" src= {crises} style={{height: '16rem', width: '16rem'}} />
                </Card.Body>
              </Card>
            </Link>
          </Col>

          <Col className="col-md-4">
            <Link style={{textDecoration: 'none'}} to="charities">
              <Card style={{objectFit:'cover', objectPosition:'center', overflow:'hidden', height:'23rem', borderRadius:'15px'}}>
                <Card.Body>
                  <Card.Title><b>{"Charities"}</b></Card.Title>
                  <Card.Subtitle>{"Contribute to relief and aid organizations"}</Card.Subtitle>
                  <Card.Img variant="bottom" src= {relief} style={{height: '16rem', width: '16rem'}} />
                </Card.Body>
              </Card>
            </Link>
          </Col>
      </Row>
    </Container>
    </div>
  );
}

export default HomePage;
