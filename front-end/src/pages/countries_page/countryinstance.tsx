import MainNavBar from '../../components/navbar/navbar';
import { TableCell, TableRow, TableBody, TableContainer, Table } from "@mui/material/";
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axiosConfig from '../../axiosConfig';
import { comma_format } from '../../utility/utility';
import GoogleMapReact from 'google-map-react';
import { Button } from "@mui/material";

function addRow(countryInfo: any, label: string, field: string, commafy_val=false){
  if (countryInfo != null) {
    var output = countryInfo[field]
    if (field === "capital") {
      output = output.replace(/\[/g, '');
      output = output.replace(/\]/g, '');
      output = output.replace(/'/g, '');
    } else if (field === "unMember") {
      output = output ? "Yes" : "No"
    }

    return countryInfo && 
    <TableRow>
      <TableCell> {label} </TableCell>
      <TableCell> {(output instanceof Array) ? output.join(", ") : commafy_val ? comma_format(output) : output}</TableCell>
    </TableRow>
  }
}

function addMap(countryInfo: any) {
  //converts capitalLatLong to float array
  let output = countryInfo.capitalLatLong.replace('[', '')
  output = output.replace(']', '')
  const myArray = output.split(",");
  // console.log(myArray)
  var coor = [parseFloat(myArray[0]), parseFloat(myArray[1])]
  // console.log(coor)
  const props = {
    center: {
      lat: coor[0],
      lng: coor[1]
    },
    zoom: 6
  };

  return (
    <div style={{ height: '40vh', width: '75%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyDmCANhvJyc0X-F05AHmGckBiqNv1-AyXE" }}
        defaultCenter={props.center}
        defaultZoom={props.zoom}
      >
      </GoogleMapReact>
    </div>
  );
}

function CountryInstancePage() {
  var params = useParams();
  // console.log(params)

  const [countryInfo, setCountryInfo] = useState<any>();
  const [crisisInfo, setCrisisInfo] = useState<any>();
  const [charityInfo, setCharityInfo] = useState<any>();

    //Stores the country info (e.g. name, capital, currency)
    
  useEffect(() => {
      const fetchCountryData = async () => {
        const result = await axiosConfig(
          "/countries/" + params.id,
        );
        setCountryInfo(result.data)

        const getCrisis = await axiosConfig("/crises/?like=%" + result.data["name"] + "%");
        var crises = getCrisis.data;
        setCrisisInfo(crises[0].ID)

        const getCharity = await axiosConfig("/charities/?like=%" + result.data["name"] + "%");
        var charity = getCharity.data;
        setCharityInfo(charity[0].projectID)
      };
      fetchCountryData();

    }, [params.id]);
    
  

  return (
    <div className="App">
      <MainNavBar />
      <h1> {params.id} </h1>
      <div>

      <TableContainer>
        <Table>
            <TableBody>
                {/* {addRow(countryInfo, "Name", "name")} */}
                {addRow(countryInfo, "Capital(s)", "capital")}
                {addRow(countryInfo, "Population", "population", true)}
                {addRow(countryInfo, "UN Member", "unMember")}
                {addRow(countryInfo, "Region", "area")}

                <TableRow>
                  <TableCell> {"Flag"} </TableCell>
                  <TableCell>
                    <img src={countryInfo && countryInfo["flag"]}
                      alt={countryInfo && ("Flag of " + countryInfo["name"])}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell> {"Map"} </TableCell>
                  <TableCell> {countryInfo && addMap(countryInfo)} </TableCell>
                </TableRow>
            </TableBody>
        </Table>
      </TableContainer>
      <Button style={{ width: "100px", height: "55px"}} variant="contained" component={Link} to={'/crises/' + crisisInfo +'/'}> Related Crisis
      </Button>
      <Button style={{ width: "100px", height: "55px"}} variant="contained" component={Link} to={'/charities/' + charityInfo +'/'}> Related Charity
      </Button>
      </div>
    </div>
  );
}

export default CountryInstancePage;
