import MainNavBar from '../../components/navbar/navbar';
import CountryTable from '../../components/country-table/country-table';

function CountriesPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Countries</h1>
      <CountryTable />
    </div>
  );
}

export default CountriesPage;
