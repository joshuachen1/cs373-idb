import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import flag from "../../media/yemenflag.png";
import map from "../../media/yemenmap.png";

function YemenPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Republic of Yemen</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Yemen-Flood">Crises</Nav.Link></Nav>
        <Nav className="justify-content-center"><Nav.Link href="AFCAids">Charities</Nav.Link></Nav>
      </p>
      <p>Capital of Yemen: Sana'a</p>
      <p>Area: Western Asia</p>
      <p>Population: 29,825,968</p>
      <p>Is Yemen part of the UN: true</p>
      <p>The country is bordered with: OMN,SAU</p>
      <img style={{width:"30%", height:"30%"}} src={flag} alt=""/>
      <img style={{width:"30%", height:"30%"}} src={map} alt=""/>
    </div>
  );
}

export default YemenPage;
