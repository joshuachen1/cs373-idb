import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import flag from "../../media/Flag_of_Zimbabwe.png";
import map from "../../media/zimbabwemap.png";

function ZimbabwePage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Republic of Zimbabwe</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Zimbabwe-Epidemic">Crises</Nav.Link></Nav>
        <Nav className="justify-content-center"><Nav.Link href="AFCAids">Charities</Nav.Link></Nav>
      </p>
      <p>Capital of Zimbabwe: Harare</p>
      <p>Area: Southern Africa</p>
      <p>Population: 14,862,927</p>
      <p>Is Zimbabwe part of the UN: true</p>
      <p>The country is bordered with: BWA,MOZ,ZAF,ZMB</p>
      <img style={{width:"30%", height:"30%"}} src={flag} alt=""/>
      <img style={{width:"30%", height:"30%"}} src={map} alt=""/>
    </div>
  );
}

export default ZimbabwePage;
