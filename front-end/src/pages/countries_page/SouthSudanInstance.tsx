import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import flag from "../../media/southsudanflag.png";
import map from "../../media/southsudanmap.png";

function SouthSudanPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Republic of South Sudan</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Sudan-Flood">Crises</Nav.Link></Nav>
        <Nav className="justify-content-center"><Nav.Link href="Kids%20for%20Kids">Charities</Nav.Link></Nav>
      </p>
      <p>Capital of South Sudan: Juba</p>
      <p>Area: Middle Africa</p>
      <p>Population: 11,193,729</p>
      <p>Is South Sudan part of the UN: true</p>
      <p>The country is bordered with: CAF,COD,ETH,KEN,SDN,UGA</p>
      <img style={{width:"30%", height:"30%"}} src={flag} alt=""/>
      <img style={{width:"30%", height:"30%"}} src={map} alt=""/>
    </div>
  );
}

export default SouthSudanPage;
