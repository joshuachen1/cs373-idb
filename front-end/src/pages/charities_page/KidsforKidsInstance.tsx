import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import pic from "../../media/kidsforkids.png";

function KidsforKidsPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Kids for Kids</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Sudan-Flood">Crises</Nav.Link></Nav>
      </p>
      <p><Nav className="justify-content-center">Countries they help: <Nav.Link href="Republic%20of%20South%20Sudan">South Sudan</Nav.Link></Nav>
      </p>
      <p>Need: Food, Goats</p>
      <p>Number of Donations: 1317</p>
      <p>Last Report: 2022-07-06</p>
      <p>Summary: All our projects are sustainable and enable families to stay in their own homes. Our Goat Loan has been called the "best microfinance scheme ever". A family is lent 5 nanny goats, and a billy goat shared between 3 families. After 2 years when the flock has grown, 5 kid goats are passed on to benefit another family in the village! Each little flock provides milk for children and an income for mothers who can sell surplus milk and yoghurt at market. Donkeys provide transport and help carry goods.</p>
      <p>Donation Link: https://www.globalgiving.org/projects/darfur-goats-and-donkeys/</p>
      <img style={{width:"30%", height:"30%"}} src={pic} alt=""/>
      <p></p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/gVyI0jTD-9M" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
  );
}

export default KidsforKidsPage;
