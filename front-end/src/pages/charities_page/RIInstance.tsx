import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import pic from "../../media/rilogo.jpg";

function RIPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Relief International</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Yemen-Flood">Crises</Nav.Link></Nav>
      </p>
      <p><Nav className="justify-content-center">Countries they help: Afghanistan, Bangladesh, Ghana, Iran, Iraq, Jordan, Lebanon, Myanmar, Pakistan, Somalia, Sudan, United States, South Sudan, <Nav.Link href="Republic%20of%20Yemen">Yemen</Nav.Link></Nav>
      </p>
      <p>Need: Food</p>
      <p>Number of Donations: 1109</p>
      <p>Last Report: 2021-02-13</p>
      <p>Summary: Two-thirds of Yemeni families have no access to healthcare and half of them have not had food of any kind in their homes for the past month. Most families in these areas regularly rely on help from a friend or relative to eat. RI's team in Yemen works to undo the harmful effects of malnutrition by providing nutritional assistance to remote corners of the country.</p>
      <p>Donation Link: https://www.globalgiving.org/projects/ri-protecting-yemens-children-from-famine/</p>
      <img style={{width:"30%", height:"30%"}} src={pic} alt=""/>
      <p></p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/yN11nLQxoQw" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
  );
}

export default RIPage;
