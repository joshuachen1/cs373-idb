import MainNavBar from '../../components/navbar/navbar';
import { TableCell, TableRow, TableBody, TableContainer, Table } from "@mui/material/";
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axiosConfig from '../../axiosConfig';
import { comma_format } from '../../utility/utility';
import GoogleMapReact from 'google-map-react';
import { Button } from "@mui/material";

function addRow(charityInfo: any, label: string, field: string, commafy_val=false){
  return charityInfo && 
  <TableRow>
    <TableCell> {label} </TableCell>
    <TableCell> {(charityInfo[field] instanceof Array) ? charityInfo[field].join(", ") : commafy_val ? comma_format(charityInfo[field]) : charityInfo[field]}</TableCell>
  </TableRow>
}

function addHeader(charityInfo: any, field: string){
  return charityInfo && 
  <h1>
    {charityInfo[field]}
  </h1>
}

function addMap(charityInfo: any) {
  const props = {
    center: {
      lat: parseFloat(charityInfo.latitude),
      lng: parseFloat(charityInfo.longitude)
    },
    zoom: 6
  };

  return (
    <div style={{ height: '40vh', width: '75%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyDmCANhvJyc0X-F05AHmGckBiqNv1-AyXE" }}
        defaultCenter={props.center}
        defaultZoom={props.zoom}
      >
      </GoogleMapReact>
    </div>
  );
}

function CharityInstancePage() {
  var params = useParams();
  // console.log(params)

  const [charityInfo, setCharityInfo] = useState<any>();
  const [countryInfo, setCountryInfo] = useState<any>();
  const [crisisInfo, setCrisisInfo] = useState<any>();
//Stores the charity info (e.g. name, capital, currency)
    

    useEffect(() => {
        const fetchCharityData = async () => {
          const result = await axiosConfig(
            "/charities/" + params.id,
          );
          setCharityInfo(result.data)

          const getCountries = await axiosConfig("/countries/?like=%" + result.data["projCountry"] + "%");
          var countries = getCountries.data;
          setCountryInfo(countries[0].name)

          const getCrisis = await axiosConfig("/crises/?like=%" + result.data["projCountry"] + "%");
          var crises = getCrisis.data;
          setCrisisInfo(crises[0].ID)
        };
        fetchCharityData();

      }, [params.id]);

  return (
    <div className="App">
      <MainNavBar />
      {addHeader(charityInfo, "projTitle")}
      <div>

      <TableContainer>
        <Table>
            <TableBody>
                {addRow(charityInfo, "Charity", "orgName")}
                {addRow(charityInfo, "Charity URL", "orgURL")}
                {addRow(charityInfo, "Charity mission", "orgMission")}
                {addRow(charityInfo, "Country", "projCountry")}
                {addRow(charityInfo, "Amount of Donations Given", "numDonations", true)}
                {addRow(charityInfo, "Project Theme", "projTheme")}
                {/* {addRow(charityInfo, "Project Title", "projTitle")} */}
                {addRow(charityInfo, "Project Summary", "projSummary")}
                {addRow(charityInfo, "Donate here", "donationURL")}

                <TableRow>
                  <TableCell> {"Charity work"} </TableCell>
                  <TableCell>
                    <img src={charityInfo && charityInfo["projImage"]}
                      alt={charityInfo && ("Image of charity work")}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell> {"Map"} </TableCell>
                  <TableCell> {charityInfo && addMap(charityInfo)} </TableCell>
                </TableRow>
            </TableBody>
        </Table>
      </TableContainer>
      <Button style={{ width: "100px", height: "55px"}} variant="contained" component={Link} to={'/countries/' + countryInfo +'/'}> Related Country
      </Button>
      <Button style={{ width: "100px", height: "55px"}} variant="contained" component={Link} to={'/crises/' + crisisInfo +'/'}> Related Crisis
      </Button>
      </div>
    </div>
  );
}

export default CharityInstancePage;
