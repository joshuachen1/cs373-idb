import MainNavBar from '../../components/navbar/navbar';
import CharityTable from '../../components/charity-table/charity-table';

function CharitiesPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Charities</h1>
      <CharityTable />
    </div>
  );
}

export default CharitiesPage;
