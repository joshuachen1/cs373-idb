import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import pic from "../../media/AFCAidslogo.png";

function AFCAidsPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>American Foundation for Children with AIDS</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Zimbabwe-Epidemic">Crises</Nav.Link></Nav>
      </p>
      <p><Nav className="justify-content-center">Countries they help: Democratic Republic of the Congo, Kenya, Malawi, Uganda,<Nav.Link href="Republic%20of%20Zimbabwe">Zimbabwe</Nav.Link></Nav>
      </p>
      <p>Need: Food</p>
      <p>Number of Donations: 749</p>
      <p>Last Report: 2022-06-21</p>
      <p>Summary: Older orphaned children and their guardian will receive training so they can grow and eat nutritious food through a viable, sustainable livelihood activity - growing veggies and fruit and raising animals to produce eggs, milk and meat. This project provides a medium and long term solution to hunger, allowing orphaned households to eat well while learning how to provide for themselves.</p>
      <p>Donation Link: https://www.globalgiving.org/projects/food-for-orphan-families/</p>
      <img style={{width:"40%", height:"40%"}} src={pic} alt=""/>
      <p></p>
      <iframe width="560" height="315" src="https://www.youtube.com/embed/ZmUNqD03DDs" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
  );
}

export default AFCAidsPage;
