import MainNavBar from '../../components/navbar/navbar';
import { TableCell, TableRow, TableBody, TableContainer, Table } from "@mui/material/";
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axiosConfig from '../../axiosConfig';
import { comma_format } from '../../utility/utility';
import GoogleMapReact from 'google-map-react';
import { Button } from "@mui/material";

function addRow(crisisInfo: any, label: string, field: string, commafy_val=false){
  return crisisInfo && 
  <TableRow>
    <TableCell> {label} </TableCell>
    <TableCell> {(crisisInfo[field] instanceof Array) ? crisisInfo[field].join(", ") : commafy_val ? comma_format(crisisInfo[field]) : crisisInfo[field]}</TableCell>
  </TableRow>
}

function addHeader(crisisInfo: any, field: string){
  return crisisInfo && 
  <h1>
    {crisisInfo[field]}
  </h1>
}

function addMap(crisisInfo: any) {
  const props = {
    center: {
      lat: parseFloat(crisisInfo.latitude),
      lng: parseFloat(crisisInfo.longitude)
    },
    zoom: 6
  };

  return (
    <div style={{ height: '40vh', width: '75%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyDmCANhvJyc0X-F05AHmGckBiqNv1-AyXE" }}
        defaultCenter={props.center}
        defaultZoom={props.zoom}
      >
      </GoogleMapReact>
    </div>
  );
}

function CrisisInstancePage() {
  var params = useParams();

  const [crisisInfo, setCrisisInfo] = useState<any>();
  const [countryInfo, setCountryInfo] = useState<any>();
  const [charityInfo, setCharityInfo] = useState<any>();
    useEffect(() => {
        const fetchCrisisData = async () => {
          const result = await axiosConfig(
            "/crises/" + params.id,
          );
          setCrisisInfo(result.data)

          const getCountries = await axiosConfig("/countries/?like=%" + result.data["country"] + "%");
          var countries = getCountries.data;
          setCountryInfo(countries[0].name)

          const getCharity = await axiosConfig("/charities/?like=%" + result.data["country"] + "%");
          var charity = getCharity.data;
          setCharityInfo(charity[0].projectID)
        };
        fetchCrisisData();
      }, [params.id]);

  return (
    <div className="App">
      <MainNavBar />
      <h1> {addHeader(crisisInfo, "name")} </h1>
      <div>

      <TableContainer>
        <Table>
            <TableBody>
                {/* {addRow(crisisInfo, "Name", "name")} */}
                {addRow(crisisInfo, "Disaster Type", "issueType")}
                {addRow(crisisInfo, "Country", "country", true)}
                {addRow(crisisInfo, "Start Date", "startingDate")}
                {addRow(crisisInfo, "Last Update", "dateLastUpdated")}
                {addRow(crisisInfo, "Overview", "overview")}
                {addRow(crisisInfo, "More Info", "source")}
                {addRow(crisisInfo, "Recent Country Report", "recentCountryReportLink")}
                {addRow(crisisInfo, "Report Author", "reportAuthor")}

                <TableRow>
                  <TableCell> {"Report Cover"} </TableCell>
                  <TableCell>
                    <img src={crisisInfo && crisisInfo["reportCover"]}
                      alt={crisisInfo && ("Report Cover")}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell> {"Report Author Logo"} </TableCell>
                  <TableCell>
                    <img src={crisisInfo && crisisInfo["authorLogo"]}
                      alt={crisisInfo && ("Author Logo")}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell> {"Map"} </TableCell>
                  <TableCell> {crisisInfo && addMap(crisisInfo)} </TableCell>
                </TableRow>
            </TableBody>

        </Table>
      </TableContainer>
      <Button style={{ width: "100px", height: "55px"}} variant="contained" component={Link} to={'/countries/' + countryInfo +'/'}> Related Country
      </Button>
      <Button style={{ width: "100px", height: "55px"}} variant="contained" component={Link} to={'/charities/' + charityInfo +'/'}> Related Charity
      </Button>
      </div>
    </div>
  );
}

export default CrisisInstancePage;
