import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import pic from "../../media/yemen-flood-2020.jpg";
import map from "../../media/yemencrisis.png"

function YemenFloodPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Republic of Yemen: Flood</h1>
      <p>
        {/* <Nav className="justify-content-center"><Nav.Link href="Republic%20of%20Yemen">Countries</Nav.Link></Nav> */}
        <Nav className="justify-content-center"><Nav.Link href="RI">Charity</Nav.Link></Nav>
      </p>
      <p>Crisis Type: Flood</p>
      <p><Nav className="justify-content-center"><Nav.Link href="Republic%20of%20Yemen">Country: Yemen</Nav.Link></Nav></p>
      <p>Start date: 2022-07-14</p>
      <p>Last updated: 2022-09-15</p>
      <p>Overview: Following heavy rains on 5 June, several districts in Ta’iz Governorate were affected by floods. According to national NGOs that conducted initial assessments in flood-affected districts, more than 2,800 households (approximately 16,800 people) in At Ta’iziyah, Mawiyah, Dimnat Khdair, Maqbanah and Sami’ districts were affected by floods. Displaced people in Al Hodeidah Governorate faced heavy rains and floods, with their shelters and food supplies destroyed. According to initial assessment conducted by CCCM partners, more than 2,900 displaced households across 22 sites in Abs, Aslam, Khayran Al Muharraq and Bani Qays districts of Hajjah Governorate and 238 households in 13 displacement sites in Az Zuhrah District of Al Hodeidah Governorate were affected. In Ad Dali’ Governorate, humanitarian partners reported some 470 households in 11 sites in Ad Dali’ City and Qa’tabah District of Ad Dali’ Governorate were affected by floods in late June.</p>
      <p>By the end of August, an estimated 51,000 families (more than 300,000 people) — mostly in displacement sites and settlements – were affected across 146 districts in 18 governorates, according to field reports from authorities and partners. Flooding caused destruction of property, farms and livelihoods, damage to critical infrastructure such as roads and shelters for internally displaced people (IDP) and, in some areas, human death. Flooding, just like the severe drought in the first half of the season greatly exacerbates food insecurity in a country where up to 19 million are food insecure. Field reports indicate that flooding has also moved unexploded ordnance to residential and agricultural areas, posing a grave risk to civilians, especially children.</p>
      <img style={{width:"30%", height:"30%"}} src={pic} alt="Photograph of Yemen Flood"/>
      <p></p>
      <img style={{width:"30%", height:"30%"}} src={map} alt="Map of Yemen Flood"/>
    </div>
  );
}

export default YemenFloodPage;
