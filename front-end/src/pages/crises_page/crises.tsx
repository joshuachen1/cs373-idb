import MainNavBar from '../../components/navbar/navbar';
import CrisisTable from '../../components/crisis-table/crisis-table';

function CrisesPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Crises</h1>
      <CrisisTable />
    </div>
  );
}

export default CrisesPage;
