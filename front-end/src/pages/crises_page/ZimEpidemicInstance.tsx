import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import pic from "../../media/zimbabwemeasles.jpg";
import map from "../../media/zimbabwecrisis.png"

function ZimEpidemicPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Zimbabwe: Epidemic</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="AFCAids">Charity</Nav.Link></Nav>
      </p>
      <p>Crisis Type: Epidemic</p>
      <p><Nav className="justify-content-center"><Nav.Link href="Republic%20of%20Zimbabwe">Country: Zimbabwe</Nav.Link></Nav></p>
      <p>Start date: 2022-09-15</p>
      <p>Last updated: 2022-09-15</p>
      <p>Overview: Zimbabwe has been hit by a deadly national measles outbreak. Cases have doubled from 1036 to 2056 in four days (as of the 15th of August 2022) and have killed 157 children from 2056 cases reported in all the provinces between April and August 20221. The disease was first recorded in Manicaland province on the 10th of April 2022 in Mutasa district. The government responded by initiating the vaccination of children under 5 years regardless of vaccination status. As of 31 August 2022, a cumulative total of 5,735 Cases were recorded by MoH. 4,117 Recoveries and 639 Deaths have been reported since the onset of the outbreak. New cases were reported from Matabeleland North (60), Manicaland (49), Mashonaland East (41), Mashonaland West (23), Chitungwiza (11), and Harare (6). Since mid-August, it has been an upward trend of new cases and a spread to other provinces. On 31 August, 639 dead were recorded against 704 in September. On 06 September, the total number of deaths was 704.</p>
      <img style={{width:"30%", height:"30%"}} src={pic} alt="Photograph of Zimbabwe Epidemic"/>
      <p></p>
      <img style={{width:"30%", height:"30%"}} src={map} alt="Map of Zimbabwe Epidemic"/>
    </div>
  );
}

export default ZimEpidemicPage;
