import MainNavBar from '../../components/navbar/navbar';
import { Nav } from 'react-bootstrap'
import pic from "../../media/southsudanflood.jpg";
import map from "../../media/southsudancrisis.png"

function SudanFloodPage() {
  return (
    <div className="App">
      <MainNavBar />
      <h1>Republic of South Sudan: Flood</h1>
      <p>
        <Nav className="justify-content-center"><Nav.Link href="Kids%20for%20Kids">Charity</Nav.Link></Nav>
      </p>
      <p>Crisis Type: Flood</p>
      <p><Nav className="justify-content-center"><Nav.Link href="Republic%20of%20South%20Sudan">Country: South Sudan</Nav.Link></Nav></p>
      <p>Start date: 2022-07-14</p>
      <p>Last updated: 2022-09-19</p>
      <p>Overview: On 10 July, flash floods triggered by torrential rains in areas south-east of Kassala town have affected an estimated 750 people in the Ashwaei area located in southern parts of Fatu, Al-Aitma and Al-Shuhada neighborhoods. At least five tukuls (huts) in the area were destroyed, according to the Government’s Humanitarian Aid Commission and the Sudanese Red Crescent Society (SRCS) that visited the area recently. In the eastern and northern parts of Al-Shuhada neighbourhood, some houses are still surrounded by water, and other houses are damaged. According to preliminary reports, emergency shelter, non-food relief supplies, and food assistance are the priority needs of the affected families. The area Inter-Sector Coordination Group (ISCG) convened on 13 July 2022 and agreed with sectors to conduct registration, verification of affected people, and the provision of emergency response.</p>
      <p>Seasonal rains and flash floods have affected about 299,500 people as of 19 September, according to the Government’s Humanitarian Aid Commission (HAC), humanitarian organiыations on the ground and local authorities. The rains and floods have destroyed at least 17,600 homes and damaged another 45,100 in 16 of the 18 states. The National Council for Civil Defence reported that 129 people died and 120 people were injured since the beginning of the rainy season in June.</p>
      <img style={{width:"40%", height:"40%"}} src={pic} alt="Photograph of Sudan Flood"/>
      <p></p>
      <img style={{width:"30%", height:"30%"}} src={map} alt="Map of Sudan Flood"/>
    </div>
  );
}

export default SudanFloodPage;
