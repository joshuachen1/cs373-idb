import MainNavBar from '../components/navbar/navbar';
import RosterTable from '../components/roster-table/roster-table';
import APITable from '../components/api-table/api-table';
import ToolTable from '../components/tools-table/tools-table';


function AboutPage() {
  return (
    <div>
      <MainNavBar />
      <h1 style={{textAlign: "center", paddingTop: '10px'}}>Mission Statement</h1>
      <p style={{textAlign: "left", paddingLeft: '16%', paddingRight: '15%'}}>Our mission is to provide users information about ongoing crises around the world, such 
        as natural disasters, war-related events, and disease-related events. We wish to provide 
        users information on which countries are facing these crises and charities/aid organizations 
        they may be able to contribute to provide relief. Our intended users are people who wish to
         become more informed across the world and see the individual challenges that each country
         is currently facing to become a better global citizen.
        </p>
      <h1 style={{textAlign: "center"}}>Our Team</h1>
      <RosterTable />
      <h1 style={{textAlign: "center"}}>Our API and APIs Used</h1>
      <APITable />
      <h1 style={{textAlign: "center", paddingTop: '20px'}}>Tools Used</h1>
      <ToolTable />
    </div>
  );
}

export default AboutPage;
