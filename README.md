# Video Demonstration
https://www.youtube.com/watch?v=A5zHlhzYGWE

## Canvas/Discord group number:
10-7


## Team members (name, eid, gitlab id):
Asim Adhikari, aa86222, asim2002

Joshua Chen, jc89873, joshuachen1,

Marlowe Johnson, msj879, msj879,

Khoi Nguyen, ktn694, khoi-t-nguyen

## Milestone 4 Git SHA
153125277a56278ddf21b563f942894ed7be319b

## Name of the project:
ReliefMaps

## URL of the GitLab repo:
https://gitlab.com/joshuachen1/cs373-idb

## Phase 4 Leader:
Marlowe Johnson

## Gitlab Pipelines:
https://gitlab.com/joshuachen1/cs373-idb/-/pipelines

## Link to website:
https://www.reliefmaps.me/

## Postman:
https://documenter.getpostman.com/view/23609847/2s83tJGqYj#cb5625d0-7278-479d-b309-6e85d7834d98

## Hours (Estimated/Actual):
Asim Adhikari (5/25)

Joshua Chen (12/12)

Marlowe Johnson (5/10)

Khoi Nguyen (5/5)

## Comments:
Put comments for referenced code at the top of files.
Referenced ReadySetPet and Endangered Nature for visualizations.
Referenced Queue for multiple filter code.
Referenced previous semesters for various test code and Gitlab CI/CD code.


## The proposed project:
In our project, we will make a website to track ongoing disasters across the world, such as natural disasters, wars-related events, and disease. The website will allow users to find ongoing disasters in various countries. The website will also list charities or international relief organizations that are relevant to the disaster. The website’s primary goal is to help keep users informed on worldwide events that may have devastating impacts on certain areas and provide them ways to contribute to aid in those areas.


## URLs of at least three disparate data sources:

- https://github.com/lennertVanSever/graphcountries
- https://data.humdata.org/dataset?q=natural%20disaster&sort=score%20desc,%20if(gt(last_modified,review_date),last_modified,review_date)%20desc&ext_page_size=25#dataset-filter-start
- https://charityapi.orghunter.com/charity-api-list


# Three models and number of instances:
| Countries | Crises | Charity/Aid Organizations |
| ------ | ------ | ------ |
| ~200 | ~20000 | ~5000 |


## Sortable/Filterable Attributes
| Countries | Crises | Charity/Aid Organizations |
| ------ | ------ | ------ |
| Population | Type | Income amount |
| Latitude/Longitude | Casualties | Asset amount |
| GDP per capita | Country | Religious affiliation |
| Size/Area | Size of area affected | Member amount |
| Continent | People in need | Overhead cost |


## Searchable Attributes
| Countries | Crises | Charity/Aid Organizations |
| ------ | ------ | ------ |
| Name of country | Name of crisis | Country based in |
| Adjacent countries | Adjacent countries | City based in |
| Type(s) of ongoing disaster | Date of crisis | Cause |
| Capital City | Location of crisis | Website link |
| Climate Data | Charities involved | Organization description |


## Media
| Countries | Crises | Charity/Aid Organizations |
| ------ | ------ | ------ |
| Line graph showing the amount of crises in a specific country over time | Bar graph comparing casualty/injury amounts between different crises | A map of countries this Aid Organization operates in |
| Bar graph showing the relative frequencies of different kinds of natural disasters | Heatmap of the world that shows the frequency of ongoing disasters | A feed of news articles about the organization |


## Questions
- How many crises is this Charity/Aid Organization currently taking place in?
- What are the challenges each country is currently facing from their crises?
- What kinds of crises are going on in the world right now and how can I help?

