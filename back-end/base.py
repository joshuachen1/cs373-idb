from flask import Flask
import json
import requests
from flask import jsonify
from flask_cors import CORS, cross_origin

api = Flask(__name__)
CORS(api)
# base_url = "/http://localhost:5000"


@api.route("/")
def my_profile():
    response_body = {
        "name": "Test message",
        "about": "Hello! I'm a CS student that is trying to sync backend and frontend stuff.",
    }
    return jsonify(response_body)


# Gets all info of every country. E.g.
@api.route("/countries_all/")
def countries_all():
    url = "https://restcountries.com/v3.1/all"
    response = requests.get(url)
    info = json.loads(response.text)

    country_list = []
    for country in info:
        country_list.append(country)
    report = {}
    report["Countries: "] = tuple(country_list)
    return jsonify(
        report
    )  # using this, can populate each row of countries page individually with get_country


# Gets all info of every country. E.g.
@api.route("/countries_all/<start_idx>-<stop_idx>")
def countries_all_by_idx(start_idx, stop_idx):
    url = "https://restcountries.com/v3.1/all"
    response = requests.get(url)
    info = json.loads(response.text)

    start_idx, stop_idx = int(start_idx), int(stop_idx)

    country_list = []
    for country in info[start_idx:stop_idx]:
        country_list.append(country)
    report = {}
    report["Countries: "] = tuple(country_list)
    return jsonify(
        report
    )  # using this, can populate each row of countries page individually with get_country


# Gets common names of country. E.g.
@api.route("/countries_common/")
def countries_common():
    url = "https://restcountries.com/v3.1/all"
    response = requests.get(url)
    info = json.loads(response.text)

    country_list = []
    for country in info:
        country_list.append(country["name"]["common"])
    report = {}
    report["Countries: "] = tuple(country_list)
    return jsonify(
        report
    )  # using this, can populate each row of countries page individually with get_country


@api.route("/countries_official/")
def countries_official():
    url = "https://restcountries.com/v3.1/all"
    response = requests.get(url)
    info = json.loads(response.text)

    country_list = []
    for country in info:
        country_list.append(country["name"]["official"])
    report = {}
    report["Countries: "] = tuple(country_list)
    return jsonify(
        report
    )  # using this, can populate each row of countries page individually with get_country


@api.route("/countries/<country_name>")
def get_country(country_name):
    url = "https://restcountries.com/v3.1/name/" + country_name
    response = requests.get(url)
    info = json.loads(response.text)

    country = info[0]
    report = {}
    report["Official name: "] = country["name"]["official"]
    report["Capital(s): "] = str(country["capital"])
    report["Area: "] = country["subregion"]
    report["Population: "] = str(country["population"])
    report["UN Member?: "] = str(country["unMember"])

    # For countries with no borders (mostly islands), this field does not exist
    if "borders" not in country:
        report["Bordered by: "] = ""
    else:
        report["Bordered by: "] = str(country["borders"])

    report["Flag: "] = country["flags"]["png"]
    report["Interactive map: "] = country["maps"]["googleMaps"]
    report["capitalLatLong"] = country["capitalInfo"]["latlng"]
    return jsonify(report)


@api.route("/crises/")
def crises():
    statuses = ["alert", "current"]  # todo: check for overlap
    crisis_names = []
    # crisis_links = []
    for status in statuses:
        url = (
            "https://api.reliefweb.int/v1/disasters?appname=reliefmap&filter[field]=status&filter[value]="
            + status
        )
        response = requests.get(url)
        info = json.loads(response.text)

        for crisis in info["data"]:
            crisis_names.append(crisis["fields"]["name"])
            # crisis_links.append(crisis["href"])
    report = {}
    report["Crises: "] = tuple(crisis_names)
    return jsonify(report)


@api.route("/crises/<crisis_name>")
def get_crisis(crisis_name):
    url = (
        "https://api.reliefweb.int/v1/disasters?appname=reliefmap&filter[field]=name&filter[value]="
        + crisis_name
    )
    response = requests.get(url)
    info = json.loads(response.text)
    url = info["data"][0]["href"]  # get the url with the info
    response = requests.get(url)
    info = json.loads(response.text)
    data = info["data"][0]
    report = {}
    report["Source: "] = data["fields"]["url"]
    report["Issue type: "] = data["fields"]["primary_type"]["name"]
    report["Country: "] = data["fields"]["country"][0]["name"]
    report["Starting Date: "] = data["fields"]["date"]["created"]
    report["Data last updated: "] = data["fields"]["date"]["changed"]
    report["Overview: "] = data["fields"]["profile"]["overview"]
    return jsonify(report)


@api.route("/charities/")
def charities():  # only gives 10 at a time, maybe a button to get each 10 using next project id
    url = "https://api.globalgiving.org/api/public/projectservice/all/projects/active/summary?api_key=b1ced50b-d17b-454a-b079-d6d4761cc4d0"
    response = requests.get(
        url, headers={"Content-Type": "application/json", "Accept": "application/json"}
    )  # otherwise, would be xml
    info = json.loads(response.text)
    next_id = info["projects"]["nextProjectId"]
    project_list = []

    for proj in info["projects"]["project"]:
        project_list.append(proj["id"])
    report = {}
    report["Charity projects: "] = tuple(project_list)
    report["Next Batch ID: "] = str(next_id)
    return jsonify(report)


@api.route("/charities/<curr_project_id>")
def get_charity(curr_project_id):
    url = (
        "https://api.globalgiving.org/api/public/projectservice/projects/"
        + str(curr_project_id)
        + "?api_key=b1ced50b-d17b-454a-b079-d6d4761cc4d0"
    )
    response = requests.get(
        url, headers={"Content-Type": "application/json", "Accept": "application/json"}
    )  # otherwise, would be xml
    proj = json.loads(response.text)["project"]
    report = {}
    report["Project ID: "] = str(proj["id"])
    report["Organization name: "] = proj["organization"]["name"]
    report["Organization URL: "] = proj["organization"]["url"]
    report["Organization mission: "] = proj["organization"]["mission"]
    report["Project title: "] = proj["title"]
    report["Project summary: "] = proj["summary"]
    report["Project theme: "] = proj["themeName"]
    report["Donation url: "] = proj["projectLink"]
    report["Number of donations: "] = str(proj["numberOfDonations"])
    report["Need: "] = proj["need"]
    report["Project country: "] = proj["country"]
    report["Project image: "] = proj["imageLink"]
    return jsonify(report)


# Example inputs / outputs:
# output = get_country("Sri Lanka")
# print(output)

# output = get_crisis("Yemen: Polio Outbreak - Aug 2020")
# print(output)

# output = get_charity(4011)
# print(output)

if __name__ == "__main__":
    # run app in debug mode on port 5000
    api.run(debug=True, port=5000)
