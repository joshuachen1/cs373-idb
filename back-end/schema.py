from flask_marshmallow import Marshmallow
from models import Charities, Countries, Crises
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema


class CharitiesSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Charities
        ordered = True


class CountriesSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Countries
        ordered = True


class CrisesSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Crises
        ordered = True


charities_schema = CharitiesSchema()
countries_schema = CountriesSchema()
crises_schema = CrisesSchema()
