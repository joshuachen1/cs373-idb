import app
import unittest
import json


class TestFlaskBackend(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()
        print(app)
        print(app.app)

    def testCharityAll(self):
        with self.client:
            response = self.client.get("/charities/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json[0]), 14)

    def testCharityById(self):
        with self.client:
            response = self.client.get("/charities/10053/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["projectID"], "10053")

    def testCharityByTitle(self):
        with self.client:
            response = self.client.get("/charities/10053/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(
                res_json["projTitle"],
                "Hope for begging talibe children, St-Louis Senegal",
            )

    def testCharityByTheme(self):
        with self.client:
            response = self.client.get("/charities/10053/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["projTheme"], "Child Protection")

    def testCharityCountry(self):
        with self.client:
            response = self.client.get("/charities/10053/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["projCountry"], "Senegal")

    def testCharityByName(self):
        with self.client:
            response = self.client.get("/charities/10053/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["orgName"], "Maison de la Gare")

    def testCountryAll(self):
        with self.client:
            response = self.client.get("/countries/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json[0]), 9)

    def testCountryByName(self):
        with self.client:
            response = self.client.get("/countries/malawi/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["name"], "Malawi")

    def testCountryByUNMember(self):
        with self.client:
            response = self.client.get("/countries/malawi/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["unMember"], "True")

    def testCountryByPopulation(self):
        with self.client:
            response = self.client.get("/countries/malawi/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["population"], 19129955)

    def testCountryByArea(self):
        with self.client:
            response = self.client.get("/countries/malawi/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["area"], "Eastern Africa")

    def testCountryByCapital(self):
        with self.client:
            response = self.client.get("/countries/malawi/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["capital"], "['Lilongwe']")

    def testCrisisAll(self):
        with self.client:
            response = self.client.get("/crises/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(len(res_json[0]), 15)

    def testCrisisById(self):
        with self.client:
            response = self.client.get("/crises/15360/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["ID"], "15360")

    def testCrisisByCountry(self):
        with self.client:
            response = self.client.get("/crises/15360/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["country"], "Kenya")

    def testCrisisByIssueType(self):
        with self.client:
            response = self.client.get("/crises/15360/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["issueType"], "Drought")

    def testCrisisByName(self):
        with self.client:
            response = self.client.get("/crises/15360/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["name"], "Kenya: Drought - 2014-2022")

    def testCrisisByStatus(self):
        with self.client:
            response = self.client.get("/crises/15360/")
            self.assertEqual(response.status_code, 200)
            res_json = json.loads(response.data)
            self.assertEqual(res_json["status"], "ongoing")


if __name__ == "__main__":
    unittest.main()
