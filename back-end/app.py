from flask import jsonify, request, Response
from flask import jsonify
import json
from flask_sqlalchemy import SQLAlchemy
from models import app, db, Charities, Countries, Crises
from schema import charities_schema, countries_schema, crises_schema
from sqlalchemy.sql import text, or_
import pycountry_convert as pc

PAGE_SIZE = 15

# For Charity Google Search
EDUCATION_RELATED_TERMS = []
GENDER_EQUALITY_RELATED_TERMS = []
PHYSICAL_HEALTH_RELATED_TERMS = []
ECONOMIC_GROWTH_RELATED_TERMS = []
CLIMATE_ACTION_RELATED_TERMS = []
CHILD_PROTECTION_RELATED_TERMS = []
SUSTAINABLE_AGRICULTURE_RELATED_TERMS = []
WILDLIFE_CONSERVATION_RELATED_TERMS = []
ENDING_HUMAN_TRAFFICKING_RELATED_TERMS = []
DISASTER_RESPONSE_RELATED_TERMS = []
CLEAN_WATER_RELATED_TERMS = []
ANIMAL_WARFARE_RELATED_TERMS = []
JUSTICE_AND_HUMAN_RIGHTS_RELATED_TERMS = []
MENTAL_HEALTH_RELATED_TERMS = []
RACIAL_JUSTICE_RELATED_TERMS = []
PEACE_AND_RECONCILIATION_RELATED_TERMS = []
REFUGEE_RIGHTS_RELATED_TERMS = []
FOOD_SECURITY_RELATED_TERMS = []
ARTS_AND_CULTURE_RELATED_TERMS = []
ENDING_ABUSE_RELATED_TERMS = []

# For Crises Google Search
FLOOD = []
EARTHQUAKE = []
EPIDEMIC_RELATED = []
TROPICAL_CYCLONE = []
FLASH_FLOOD = []
DROUGHT = []
OTHER = []
LAND_SLIDE = []
INSECT_INFESTATION = []


@app.route("/")
def my_profile():
    response_body = {
        "name": "Documentation",
        "about": "https://documenter.getpostman.com/view/23609847/2s83tJGqYj#3e4203b6-38ba-4e14-84da-69fe71721d83",
    }
    return jsonify(response_body)


@app.route("/all")
def all():
    response_body = {
        "name": "Test message",
        "about": "Hello! I'm a CS student that is trying to sync back and frontend stuff!",
    }
    like = request.args.get("like", type=str) or "%"
    charities_query = db.session.query(Charities).filter(
        or_(
            Charities.orgName.like(like),
            Charities.orgURL.like(like),
            Charities.orgMission.like(like),
            Charities.projTitle.like(like),
            Charities.projSummary.like(like),
            Charities.projTheme.like(like),
            Charities.projImage.like(like),
            Charities.donationURL.like(like),
            Charities.numDonations.like(like),
            Charities.need.like(like),
            Charities.projCountry.like(like),
            Charities.latitude.like(like),
            Charities.longitude.like(like),
            Charities.projectID.like(like),
        )
    )
    countries_query = db.session.query(Countries).filter(
        or_(
            Countries.name.like(like),
            Countries.area.like(like),
            Countries.capital.like(like),
            Countries.interactiveMap.like(like),
            Countries.borderedBy.like(like),
            Countries.capital.like(like),
            Countries.capitalLatLong.like(like),
            Countries.flag.like(like),
        )
    )
    crises_query = db.session.query(Crises).filter(
        or_(
            Crises.name.like(like),
            Crises.country.like(like),
            Crises.issueType.like(like),
            Crises.overview.like(like),
            Crises.source.like(like),
            Crises.status.like(like),
            Crises.ID.like(like),
            Crises.longitude.like(like),
            Crises.latitude.like(like),
            Crises.dateLastUpdated.like(like),
            Crises.startingDate.like(like),
        )
    )
    crises_result = crises_schema.dump(crises_query, many=True)
    charities_result = charities_schema.dump(charities_query, many=True)
    countries_result = countries_schema.dump(countries_query, many=True)

    # full_result = {crises_result,charities_result,countries_result}
    crises_count = crises_query.count()
    charities_count = charities_query.count()
    countries_count = countries_query.count()
    final_result = (crises_result + charities_result) + countries_result
    return jsonify(final_result)
    # return jsonify(full_result)


@app.route("/charities/")
@app.route("/charities/<int:id>/")
@app.route("/charities/<int:start_idx>-<int:stop_idx>/")
def get_charity(id=None, start_idx=0, stop_idx=0):
    if id:
        query = db.session.query(Charities).filter_by(projectID=id)
        try:
            result = charities_schema.dump(query, many=True)[0]
        except IndexError:
            return error_message("Invalid city ID: {r_id}")
    else:

        page = request.args.get("page", type=int)
        perPage = request.args.get("perPage", type=int)
        sortBy = request.args.get("sortBy", type=str) or "projectID"
        orderBy = request.args.get("orderBy", type=str) or "desc"
        like = request.args.get("like", type=str) or "%"

        filterCountry = request.args.get("projCountry", type=str)
        filterOrgName = request.args.get("orgName", type=str)
        filterDonationsGreater = request.args.get("numDonationsGreater", type=str)
        filterDonationsLess = request.args.get("numDonationsLess", type=str)
        filterDonationsEqual = request.args.get("numDonationsEqual", type=str)
        filterTheme = request.args.get("theme", type=str)
        filterSummary = request.args.get("projSummary", type=str)
        filterContinent = request.args.get("projContinent", type=str)

        query = (
            db.session.query(Charities)
            .filter(
                or_(
                    Charities.orgName.like(like),
                    Charities.orgURL.like(like),
                    Charities.orgMission.like(like),
                    Charities.projTitle.like(like),
                    Charities.projSummary.like(like),
                    Charities.projTheme.like(like),
                    Charities.projImage.like(like),
                    Charities.donationURL.like(like),
                    Charities.numDonations.like(like),
                    Charities.need.like(like),
                    Charities.projCountry.like(like),
                    Charities.latitude.like(like),
                    Charities.longitude.like(like),
                    Charities.projectID.like(like),
                ),
                filterCountry is None or Charities.projCountry == filterCountry,
                filterOrgName is None or Charities.orgName == filterOrgName,
                filterTheme is None or Charities.projTheme == filterTheme,
                filterSummary is None or Charities.projSummary.contains(filterSummary),
                # filterDonationsGreater is None or Charities.numDonations > filterDonationsGreater,
                # filterDonationsLess is None or Charities.numDonations < filterDonationsLess,
                filterDonationsEqual is None
                or Charities.numDonations == filterDonationsEqual,
            )
            .order_by(text(sortBy + " " + orderBy))
        )
        count = query.count()
        if page is not None:
            query = paginate(query, page, perPage)
        result = charities_schema.dump(query, many=True)
        if not filterContinent is None:
            # print(filterContinent)
            info_index = 0
            while info_index < len(result):
                currCountry = result[info_index]["projCountry"]
                if currCountry == "The Bahamas":
                    currCountry = "Bahamas"
                if currCountry == "Sint Maarten":
                    currCountry = "Netherlands"
                if currCountry == "Taiwan":
                    currCountry = "Taiwan, Province of China"
                if currCountry == "Democratic Republic of the Congo":
                    currCountry = "Democratic Republic of the Congo"
                if currCountry == "Republic of Moldova":
                    currCountry = "Moldova, Republic of"
                if currCountry == "The Gambia":
                    currCountry = "Gambia"
                if currCountry == "The Federated States of Micronesia":
                    currCountry = "Micronesia, Federated States of"
                if currCountry == "United Republic of Tanzania":
                    currCountry = "Tanzania, United Republic of"
                if currCountry == "Hong Kong SAR":
                    currCountry = "Hong Kong"
                # print(currCountry)
                country_code = ""
                if (
                    currCountry != "Kosovo"
                    and currCountry != "East Timor"
                    and currCountry != "Virgin Islands"
                ):
                    country_code = pc.country_name_to_country_alpha2(
                        currCountry, cn_name_format="default"
                    )
                # print(country_code)
                continent = ""
                if currCountry == "Kosovo":
                    continent = "EU"
                elif currCountry == "East Timor":
                    continent = "AS"
                elif currCountry == "Virgin Islands":
                    continent = "NA"
                elif currCountry == "Antarctica":
                    continent = "AN"
                else:
                    continent = pc.country_alpha2_to_continent_code(country_code)
                # print(continent)
                # print(filterContinent)
                if continent != filterContinent:
                    del result[info_index]
                    info_index -= 1
                info_index += 1

        if not filterDonationsGreater is None:
            # print(filterContinent)
            info_index = 0
            inputDonationsGreater = int(filterDonationsGreater)
            while info_index < len(result):
                currDonations = int(result[info_index]["numDonations"])
                if currDonations <= inputDonationsGreater:
                    del result[info_index]
                    info_index -= 1
                info_index += 1

        if not filterDonationsLess is None:
            # print(filterContinent)
            info_index = 0
            inputDonationsLess = int(filterDonationsLess)
            while info_index < len(result):
                currDonations = int(result[info_index]["numDonations"])
                if currDonations >= inputDonationsLess:
                    del result[info_index]
                    info_index -= 1
                info_index += 1
        if start_idx != 0 or stop_idx != 0:
            return jsonify(result[start_idx:stop_idx])
    return jsonify(result)


@app.route("/countries/")
@app.route("/countries/<string:cname>/")
@app.route("/countries/<int:start_idx>-<int:stop_idx>/")
def get_country(cname=None, start_idx=0, stop_idx=0):
    if cname:
        query = db.session.query(Countries).filter_by(name=cname)
        try:
            result = countries_schema.dump(query, many=True)[0]
        except IndexError:
            return error_message("Invalid country name: {r_name}")
    else:
        page = request.args.get("page", type=int)
        perPage = request.args.get("perPage", type=int)
        sortBy = request.args.get("sortBy", type=str) or "name"
        orderBy = request.args.get("orderBy", type=str) or "desc"
        like = request.args.get("like", type=str) or "%"

        filterArea = request.args.get("area", type=str)
        filterUNMember = request.args.get("unMember", type=str)
        filterBordered = request.args.get("borderedBy", type=str)
        filterPopulationGreater = request.args.get("populationGreater", type=int)
        filterPopulationLess = request.args.get("populationLess", type=int)
        filterPopulationEqual = request.args.get("populationEqual", type=int)
        filterName = request.args.get("name", type=str)

        query = (
            db.session.query(Countries)
            .filter(
                or_(
                    Countries.name.like(like),
                    Countries.area.like(like),
                    Countries.capital.like(like),
                    Countries.interactiveMap.like(like),
                    Countries.borderedBy.like(like),
                    Countries.capital.like(like),
                    Countries.capitalLatLong.like(like),
                    Countries.flag.like(like),
                ),
                filterArea is None or Countries.area == filterArea,
                filterUNMember is None or Countries.unMember == filterUNMember,
                filterBordered is None or Countries.borderedBy.contains(filterBordered),
                filterPopulationGreater is None
                or Countries.population > filterPopulationGreater,
                filterPopulationLess is None
                or Countries.population < filterPopulationLess,
                filterPopulationEqual is None
                or Countries.population == filterPopulationEqual,
                filterName is None or Countries.name == filterName,
            )
            .order_by(text(sortBy + " " + orderBy))
        )

        count = query.count()
        if page is not None:
            query = paginate(query, page, perPage)
        result = countries_schema.dump(query, many=True)
        if start_idx != 0 or stop_idx != 0:
            return jsonify(result[start_idx:stop_idx])
    return jsonify(result)


@app.route("/crises/")
@app.route("/crises/<int:num>/")
@app.route("/crises/<int:start_idx>-<int:stop_idx>/")
def get_crisis(num=None, start_idx=0, stop_idx=0):
    if num:
        query = db.session.query(Crises).filter_by(ID=num)
        try:
            result = crises_schema.dump(query, many=True)[0]
        except IndexError:
            return error_message("Invalid crisis ID: {r_ID}")
    else:
        page = request.args.get("page", type=int)
        perPage = request.args.get("perPage", type=int)
        sortBy = request.args.get("sortBy", type=str) or "ID"
        orderBy = request.args.get("orderBy", type=str) or "desc"
        like = request.args.get("like", type=str) or "%"

        filterType = request.args.get("issueType", type=str)
        filterCountry = request.args.get("country", type=str)
        filterStatus = request.args.get("status", type=str)
        filterLatitude = request.args.get("latitude", type=str)
        filterLongitude = request.args.get("longitude", type=str)

        query = (
            db.session.query(Crises)
            .filter(
                or_(
                    Crises.name.like(like),
                    Crises.country.like(like),
                    Crises.issueType.like(like),
                    Crises.overview.like(like),
                    Crises.source.like(like),
                    Crises.status.like(like),
                    Crises.ID.like(like),
                    Crises.longitude.like(like),
                    Crises.latitude.like(like),
                    Crises.dateLastUpdated.like(like),
                    Crises.startingDate.like(like),
                ),
                filterType is None or Crises.issueType == filterType,
                filterCountry is None or Crises.country == filterCountry,
                filterStatus is None or Crises.status == filterStatus
                # filterLatitude is None or Crises.latitude > filterLatitude - 4 and Crises.latitude < filterLatitude + 4,
                # filterLongitude is None or Crises.longitude > filterLongitude - 4 and Crises.longitude < filterLongitude + 4
            )
            .order_by(text(sortBy + " " + orderBy))
        )

        count = query.count()
        if page is not None:
            query = paginate(query, page, perPage)
        result = crises_schema.dump(query, many=True)
        if not filterLatitude is None:
            # print(filterContinent)
            info_index = 0
            inputLat = float(filterLatitude)
            while info_index < len(result):
                currLatitude = float(result[info_index]["latitude"])
                print(inputLat, currLatitude)
                if (currLatitude < inputLat - 4) or (currLatitude > inputLat + 4):
                    del result[info_index]
                    info_index -= 1
                info_index += 1
        if not filterLongitude is None:
            # print(filterContinent)
            info_index = 0
            inputLong = float(filterLongitude)
            while info_index < len(result):
                currLongitude = float(result[info_index]["longitude"])
                if (currLongitude < inputLong - 4) or (currLongitude > inputLong + 4):
                    del result[info_index]
                    info_index -= 1
                info_index += 1
        if start_idx != 0 or stop_idx != 0:
            return jsonify(result[start_idx:stop_idx])
    return jsonify(result)


def error_message(msg):
    resp = Response(json.dumps({"error": msg}), mimetype="application/json")
    resp.error_code = 404
    return resp


def paginate(query, page_num, num_per_page):
    if num_per_page is None:
        num_per_page = PAGE_SIZE
    return query.paginate(page=page_num, per_page=num_per_page, error_out=False).items


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=5000)
