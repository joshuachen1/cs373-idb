from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field

# host = 'reliefmap.cwmwh510qqww.us-east-1.rds.amazonaws.com'
# user = 'admin'
# port = 3306
# password = 'soccer1234'
# database = 'reliefmap'

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+pymysql://admin:soccer1234@reliefmap.cc0rqsdauk4x.us-east-1.rds.amazonaws.com:3306/reliefmap"
db = SQLAlchemy(app)
CORS(app)
ma = Marshmallow()


class Charities(db.Model):
    projectID = db.Column(db.String(), primary_key=True)
    orgName = db.Column(db.String())
    orgURL = db.Column(db.String())
    orgMission = db.Column(db.String())
    projTitle = db.Column(db.String())
    projSummary = db.Column(db.String())
    projTheme = db.Column(db.String())
    donationURL = db.Column(db.String())
    numDonations = db.Column(db.String())
    need = db.Column(db.String())
    projCountry = db.Column(db.String())
    projImage = db.Column(db.String())
    latitude = db.Column(db.String())
    longitude = db.Column(db.String())


class Countries(db.Model):
    area = db.Column(db.String())
    borderedBy = db.Column(db.String())
    capital = db.Column(db.String())
    flag = db.Column(db.String())
    interactiveMap = db.Column(db.String())
    name = db.Column(db.String(), primary_key=True)
    population = db.Column(db.Integer)
    unMember = db.Column(db.String())
    capitalLatLong = db.Column(db.String())


class Crises(db.Model):
    dateLastUpdated = db.Column(db.String())
    country = db.Column(db.String())
    issueType = db.Column(db.String())
    name = db.Column(db.String())
    overview = db.Column(db.String())
    # overviewhtml = db.Column(db.String()) #name of column is overview-html, so causes issues
    recentCountryReportLink = db.Column(db.String())
    reportCover = db.Column(db.String())
    reportAuthor = db.Column(db.String())
    authorLogo = db.Column(db.String())
    source = db.Column(db.String())
    startingDate = db.Column(db.String())
    status = db.Column(db.String())
    ID = db.Column(db.String(), primary_key=True)
    longitude = db.Column(db.String())
    latitude = db.Column(db.String())
